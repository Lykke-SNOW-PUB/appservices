﻿using System.Threading.Tasks;
using Lykke.Terminal.Domain.ApplicationServices.Exchange;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace Lykke.Terminal.WebService.Controllers
{
    [Route("api/[controller]")]
    public class TransactionHistoryController : Controller
    {
        private readonly ITransactionHistoryAppService _transactionHistoryService;

        public TransactionHistoryController(ITransactionHistoryAppService transactionHistoryService)
        {
            _transactionHistoryService = transactionHistoryService;
        }

        [HttpGet("{accountId}")]
        public async Task<string> Get(string accountId)
        {
            var transactionHistory = await _transactionHistoryService.GetTransactionHistoryAsync(accountId);
            return JsonConvert.SerializeObject(transactionHistory);
        }
    }
}