﻿using System.Threading.Tasks;
using Lykke.Terminal.Domain.ApplicationServices.Exchange;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace Lykke.Terminal.WebService.Controllers
{
    [Route("api/[controller]")]
    public class ActiveOrderController : Controller
    {
        private readonly IActiveOrderAppService _activeOrderService;

        public ActiveOrderController(IActiveOrderAppService activeOrderService)
        {
            _activeOrderService = activeOrderService;
        }

        [HttpGet("{id}")]
        public async Task<string> Get(string id)
        {
            var activeOrders = await _activeOrderService.GetActiveOrdersAsync(id);

            return JsonConvert.SerializeObject(activeOrders);
        }
    }
}