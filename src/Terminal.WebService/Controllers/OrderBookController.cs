﻿using System.Threading.Tasks;
using Lykke.Terminal.Domain.ApplicationServices.Exchange;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace Lykke.Terminal.WebService.Controllers
{
    [Route("api/[controller]")]
    public class OrderBookController : Controller
    {
        private readonly IOrderBookAppService _orderBookService;

        public OrderBookController(IOrderBookAppService orderBookService)
        {
            _orderBookService = orderBookService;
        }

        [HttpGet]
        public async Task<string> Get()
        {
            var orderBook = await _orderBookService.GetOrderBookAsync();

            return JsonConvert.SerializeObject(orderBook);
        }
    }
}