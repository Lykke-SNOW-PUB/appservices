﻿using System.Threading.Tasks;
using Lykke.Terminal.Domain.Accounts;
using Lykke.Terminal.Domain.ApplicationServices.Accounts;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace Lykke.Terminal.WebService.Controllers
{
    [Route("api/[controller]")]
    public class AccountInfoController : Controller
    {
        private readonly IAccountAppService _accountService;

        public AccountInfoController(IAccountAppService accountService)
        {
            _accountService = accountService;
        }

        [HttpGet("{id}")]
        public async Task<string> Get(string id)
        {
            var accountInfo = await _accountService.GetAccountInfoAsync(id);
            return JsonConvert.SerializeObject(accountInfo);
        }
    }
}