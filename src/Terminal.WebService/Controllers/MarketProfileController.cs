﻿using System.Threading.Tasks;
using Lykke.Terminal.Domain.ApplicationServices.Exchange;
using Lykke.Terminal.Domain.Exchange;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace Lykke.Terminal.WebService.Controllers
{
    [Route("api/[controller]")]
    public class MarketProfileController : Controller
    {
        private readonly IMarketProfileAppService _marketProfileService;

        public MarketProfileController(IMarketProfileAppService marketProfileService)
        {
            _marketProfileService = marketProfileService;
        }

        [HttpGet]
        public async Task<string> Get()
        {
            var marketProfile = await _marketProfileService.GetMarketProfileAsync();

            return JsonConvert.SerializeObject(marketProfile);
        }
    }
}