﻿using System;
using System.IO;
using Lykke.Terminal.Common.Validation;
using Lykke.Terminal.DataAccess;
using Lykke.Terminal.Domain.Settings;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;

namespace Lykke.Terminal.WebService
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var settings = ReadGeneralSettings();

            var host = new WebHostBuilder()
                .UseKestrel()
                .UseContentRoot(Directory.GetCurrentDirectory())
                .UseIISIntegration()
                .ConfigureServices(collection => collection.AddSingleton<IBaseSettings>(settings))
                .UseStartup<Startup>()
                .Build();

            host.Run();
        }

        private static IBaseSettings ReadGeneralSettings()
        {
            var settingsData = ReadSettingsFile();

            if (string.IsNullOrWhiteSpace(settingsData))
            {
                throw new Exception("Please, provide mongo_generalsettings.json file");
            }

            var settings = GeneralSettingsReader.ReadSettingsFromData<BaseSettings>(settingsData);

            GeneralSettingsValidator.Validate(settings);

            return settings;
        }

        private static string ReadSettingsFile()
        {
            try
            {
#if DEBUG
                return File.ReadAllText(string.Format("..{0}..{0}..{0}settings{0}mongo_generalsettings.json", Path.DirectorySeparatorChar));
#else
                return File.ReadAllText("mongo_generalsettings." + Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") + ".json");
#endif
            }
            catch (Exception exc)
            {
                Console.WriteLine(exc);
                throw;
            }
        }
    }
}
