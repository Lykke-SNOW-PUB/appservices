﻿using System;
using Lykke.Terminal.BusinessService;
using Lykke.Terminal.DataAccess;
using Lykke.Terminal.Domain.Messaging;
using Lykke.Terminal.Domain.Settings;
using Lykke.Terminal.Messaging;
using Microsoft.Extensions.DependencyInjection;
using RawRabbit.Configuration;
using StructureMap;

namespace Lykke.Terminal.WebService.Configurations
{
    public static class ApiDependencies
    {
        public static IServiceProvider Create(IServiceCollection services, IBaseSettings settings)
        {
            var container = new Container();

            container.Configure(
                _ =>
                {
                    _.AddRegistry(new MongoDataAccessConfig(settings));
                    _.AddRegistry(new BusinessServiceConfig(BrokerConfiguration.Default)); //todo: fix it! unneeded dependency here
                });

            container.Populate(services);

            return container.GetInstance<IServiceProvider>();
        }

    }
}