﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Lykke.Terminal.Common.Utils;
using Lykke.Terminal.DataAccess.Mongo;
using Lykke.Terminal.Domain.Assets;
using MongoDB.Bson.Serialization.Attributes;

namespace Lykke.Terminal.DataAccess.Repositories.Assets
{
    public class AssetPairEntity : MongoEntity, IAssetPair
    {
        [BsonIgnore]
        public string Id => BsonId;
        public string Name { get; set; }
        public int Accuracy { get; set; }
        public string BaseAssetId { get; set; }
        public string QuotingAssetId { get; set; }
        public int InvertedAccuracy { get; set; }
        public double SwapShort { get; set; }
        public double SwapLong { get; set; }

        public static AssetPairEntity Create(IAssetPair src)
        {
            return new AssetPairEntity
            {
                BsonId = src.Id,
                Accuracy = src.Accuracy,
                Name = src.Name,
                BaseAssetId = src.BaseAssetId,
                QuotingAssetId = src.QuotingAssetId,
                InvertedAccuracy = src.InvertedAccuracy,
                SwapShort = src.SwapShort,
                SwapLong = src.SwapLong
            };
        }
    }

    public class AssetPairRepository : IAssetPairRepository
    {
        private readonly IMongoStorage<AssetPairEntity> _tableStorage;

        public AssetPairRepository(IMongoStorage<AssetPairEntity> tableStorage)
        {
            _tableStorage = tableStorage;
        }

        public async Task<IEnumerable<IAssetPair>> GetAllAsync()
        {
            return await _tableStorage.GetDataAsync();
        }

        public Task AddAsync(IAssetPair entity)
        {
            var newEntity = AssetPairEntity.Create(entity);

            return _tableStorage.InsertAsync(newEntity);
        }

        public Task UpdateAsync(IAssetPair entity)
        {
            throw new System.NotImplementedException();
        }

        public Task DeteleAsync(string id)
        {
            throw new System.NotImplementedException();
        }

        public async Task<IAssetPair> GetByIdAsync(string id)
        {
            return await _tableStorage.GetDataAsync(id);
        }

        public async Task AddAllAsync(IEnumerable<IAssetPair> assetPairs)
        {
            foreach (var pairs in assetPairs.ToPieces(10))
            {
                var bunchArr = pairs.ToArray();
                await Task.WhenAll(
                    bunchArr.Select(
                        m => _tableStorage.InsertAsync(AssetPairEntity.Create(m))));
            }
        }

        public async Task<IAssetPair> GetByBaseAndQuotingIds(string baseAssetId, string quotingAssetId)
        {
            var assetPair =
                await
                    _tableStorage.GetDataAsync(o => o.BaseAssetId == baseAssetId && o.QuotingAssetId == quotingAssetId);

            return assetPair?.FirstOrDefault();
        }
    }
}