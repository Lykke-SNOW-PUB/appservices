﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Lykke.Terminal.DataAccess.Mongo;
using Lykke.Terminal.Domain.Accounts;
using MongoDB.Bson.Serialization.Attributes;

namespace Lykke.Terminal.DataAccess.Repositories.Accounts
{
    public class AccountInfoEntity : MongoEntity, IAccountInfo
    {
        [BsonIgnore]
        public string AccountId => BsonId;

        public static AccountInfoEntity Create(IAccountInfo src)
        {
            var accountInfoEntity = new AccountInfoEntity
            {
                BsonId = src.AccountId
            };
            accountInfoEntity.Update(src);
            return accountInfoEntity;
        }

        internal void Update(IAccountInfo entity)
        {
        }
    }

    public class AccountInfoRepository : IAccountInfoRepository
    {
        private readonly IMongoStorage<AccountInfoEntity> _tableStorage;

        public AccountInfoRepository(IMongoStorage<AccountInfoEntity> tableStorage)
        {
            _tableStorage = tableStorage;
        }

        public Task<IEnumerable<IAccountInfo>> GetAllAsync()
        {
            throw new NotImplementedException();
        }

        public Task AddAsync(IAccountInfo entity)
        {
            var newEntity = AccountInfoEntity.Create(entity);

            return _tableStorage.InsertAsync(newEntity);
        }

        public Task UpdateAsync(IAccountInfo entity)
        {
            return _tableStorage.ReplaceAsync(entity.AccountId, itm =>
            {
                itm.Update(entity);
                return itm;
            });
        }

        public Task DeteleAsync(string id)
        {
            throw new NotImplementedException();
        }

        public async Task<IAccountInfo> GetByIdAsync(string id)
        {
            return await _tableStorage.GetDataAsync(id);
        }
    }
}