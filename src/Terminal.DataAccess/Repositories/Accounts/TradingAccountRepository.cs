﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Lykke.Terminal.DataAccess.Mongo;
using Lykke.Terminal.Domain.Accounts;
using MongoDB.Bson.Serialization.Attributes;

namespace Lykke.Terminal.DataAccess.Repositories.Accounts
{
    public class TradingAccountEntity : MongoEntity, ITradingAccount
    {
        [BsonIgnore]
        public string TradingAccountId => BsonId;

        public string AccountId { get; set; }
        public double Balance { get; set; }
        public string BaseAssetId { get; set; }
        public double Leverage { get; set; }
        public double StopOut { get; set; }

        public static TradingAccountEntity Create(ITradingAccount src)
        {
            var result = new TradingAccountEntity
            {
                BsonId = Guid.NewGuid().ToString()
            };
            result.Update(src);

            return result;
        }

        internal void Update(ITradingAccount src)
        {
            AccountId = src.AccountId;
            Balance = src.Balance;
            BaseAssetId = src.BaseAssetId;
            Leverage = src.Leverage;
            StopOut = src.StopOut;
        }
    }

    public class TradingAccountRepository : Repository<ITradingAccount, TradingAccountEntity>, ITradingAccountRepository
    {
        private readonly IMongoStorage<TradingAccountEntity> _tableStorage;

        public TradingAccountRepository(IMongoStorage<TradingAccountEntity> tableStorage) : base(tableStorage)
        {
            _tableStorage = tableStorage;
        }

        public override async Task AddAsync(ITradingAccount entity)
        {
            var newEntity = TradingAccountEntity.Create(entity);
            await _tableStorage.InsertAsync(newEntity);
        }

        public override Task UpdateAsync(ITradingAccount entity)
        {
            return _tableStorage.ReplaceAsync(entity.TradingAccountId, itm =>
            {
                itm.Update(entity);
                return itm;
            });
        }

        public async Task<IEnumerable<ITradingAccount>> GetByClientId(string clientId)
        {
            return await _tableStorage.GetDataAsync(x => x.AccountId == clientId);
        }
    }
}