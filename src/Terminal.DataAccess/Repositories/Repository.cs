﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Lykke.Terminal.DataAccess.Mongo;
using Lykke.Terminal.Domain.Infrastructure;

namespace Lykke.Terminal.DataAccess.Repositories
{
    public class Repository<TDomain, TEntity> : IRepository<TDomain> where TEntity : MongoEntity, TDomain
    {
        private readonly IMongoStorage<TEntity> _tableStorage;

        protected Repository(IMongoStorage<TEntity> tableStorage)
        {
            _tableStorage = tableStorage;
        }

        public virtual async Task<IEnumerable<TDomain>> GetAllAsync()
        {
            return await _tableStorage.GetDataAsync();
        }

        public virtual Task AddAsync(TDomain entity)
        {
            throw new System.NotImplementedException();
        }

        public virtual Task UpdateAsync(TDomain entity)
        {
            throw new System.NotImplementedException();
        }

        public virtual async Task DeteleAsync(string id)
        {
            await _tableStorage.DeleteAsync(id);
        }

        public virtual async Task<TDomain> GetByIdAsync(string id)
        {
            return await _tableStorage.GetDataAsync(id);
        }
    }
}