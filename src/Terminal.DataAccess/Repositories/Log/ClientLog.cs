﻿using System;
using System.Threading.Tasks;
using Lykke.Terminal.DataAccess.Mongo;
using Lykke.Terminal.Domain.Log;

namespace Lykke.Terminal.DataAccess.Repositories.Log
{
    public class ClientLogItem : MongoEntity
    {
        public string Data { get; set; }

        public static ClientLogItem Create(string userId, string data)
        {
            return new ClientLogItem
            {
                BsonId = userId,
                Data = data
            };
        }
    }

    public class ClientLog : IClientLog
    {
        private readonly IMongoStorage<ClientLogItem> _tableStorage;

        public ClientLog(IMongoStorage<ClientLogItem> tableStorage)
        {
            _tableStorage = tableStorage;
        }

        public Task WriteAsync(string userId, string dataId)
        {
            var newEntity = ClientLogItem.Create(userId, dataId);
            return _tableStorage.InsertAndGenerateRowKeyAsDateTimeAsync(newEntity, DateTime.UtcNow);
        }
    }
}