﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Lykke.Terminal.DataAccess.Mongo;
using Lykke.Terminal.Domain.Exchange;
using MongoDB.Bson.Serialization.Attributes;

namespace Lykke.Terminal.DataAccess.Repositories.Exchange
{
    public class PendingOrderEntity : MongoEntity, IOrderBase
    {
        public OrderStatus Status { get; set; }
        public double DefinedPrice { get; set; }
        public DateTime LastModified { get; set; }
        public OrderComment Comment { get; set; }
        public double TakeProfit { get; set; }
        public double StopLoss { get; set; }
        public double Commission { get; set; }
        public string ClientId { get; set; }
        public string TradingAccountId { get; set; }
        public DateTime CreatedAt { get; set; }
        [BsonIgnore]
        public string Id => BsonId;
        public string AssetPairId { get; set; }
        public double Volume { get; set; }
        public double Price { get; set; }

        public static PendingOrderEntity Create(IOrderBase src)
        {
            return new PendingOrderEntity
            {
                BsonId = Guid.NewGuid().ToString(),
                AssetPairId = src.AssetPairId,
                ClientId = src.ClientId,
                CreatedAt = src.CreatedAt,
                Price = src.Price,
                Status = src.Status,
                Volume = src.Volume,
                TradingAccountId = src.TradingAccountId,
                DefinedPrice = src.DefinedPrice,
                LastModified = src.LastModified,
                Comment = src.Comment,
                StopLoss = src.StopLoss,
                TakeProfit = src.TakeProfit,
                Commission = src.Commission
            };
        }
    }

    public class PendingOrdersRepository : Repository<IOrderBase,PendingOrderEntity>, IPendingOrdersRepository
    {
        private readonly IMongoStorage<PendingOrderEntity> _tableStorage;

        public PendingOrdersRepository(IMongoStorage<PendingOrderEntity> tableStorage) : base(tableStorage)
        {
            _tableStorage = tableStorage;
        }

        public override async Task AddAsync(IOrderBase limitOrder)
        {
            var newEntity = PendingOrderEntity.Create(limitOrder);
            await _tableStorage.InsertAsync(newEntity);
        }

        public override Task UpdateAsync(IOrderBase entity)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<IOrderBase>> GetByAssetPairIdAsync(string assetPairId)
        {
            return await _tableStorage.GetDataAsync(x => x.AssetPairId == assetPairId);
        }
    }
}