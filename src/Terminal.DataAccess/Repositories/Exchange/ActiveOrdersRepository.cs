using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Lykke.Terminal.DataAccess.Mongo;
using Lykke.Terminal.Domain.Exchange;
using MongoDB.Bson.Serialization.Attributes;

namespace Lykke.Terminal.DataAccess.Repositories.Exchange
{
    public class ActiveOrderEntity : MongoEntity, IOrderBase
    {
        public string ClientId { get; set; }
        public string TradingAccountId { get; set; }
        public DateTime CreatedAt { get; set; }

        [BsonIgnore]
        public string Id => BsonId;

        public double Price { get; set; }
        public string AssetPairId { get; set; } //todo:add index
        public double Volume { get; set; }
        public OrderStatus Status { get; set; }
        public double DefinedPrice { get; set; }
        public DateTime LastModified { get; set; }
        public OrderComment Comment { get; set; }
        public double TakeProfit { get; set; }
        public double StopLoss { get; set; }
        public double Commission { get; set; }

        public static ActiveOrderEntity Create(IOrderBase src)
        {
            return new ActiveOrderEntity
            {
                BsonId = src.Id,
                AssetPairId = src.AssetPairId,
                ClientId = src.ClientId,
                CreatedAt = src.CreatedAt,
                Status = src.Status,
                Volume = src.Volume,
                Price = src.Price,
                TradingAccountId = src.TradingAccountId,
                Comment = src.Comment,
                TakeProfit = src.TakeProfit,
                StopLoss = src.StopLoss,
                Commission = src.Commission
            };
        }
    }

    public class ActiveOrdersRepository : Repository<IOrderBase, ActiveOrderEntity>, IActiveOrdersRepository
    {
        private readonly IMongoStorage<ActiveOrderEntity> _tableStorage;

        public ActiveOrdersRepository(IMongoStorage<ActiveOrderEntity> tableStorage) : base(tableStorage)
        {
            _tableStorage = tableStorage;
        }

        public override async Task AddAsync(IOrderBase marketOrder)
        {
            var newEntity = ActiveOrderEntity.Create(marketOrder);
            await _tableStorage.InsertAsync(newEntity);
        }

        public override Task UpdateAsync(IOrderBase entity)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<IOrderBase>> GetByClientId(string clientId)
        {
            return await _tableStorage.GetDataAsync(x => x.ClientId == clientId);
        }

        public async Task<IEnumerable<IOrderBase>> GetByTradingAccountIdAsync(string tradingAccountId)
        {
            return await _tableStorage.GetDataAsync(x => x.TradingAccountId == tradingAccountId);
        }

        public async Task<IEnumerable<IOrderBase>> GetByAssetPairIdAsync(string assetPairId)
        {
            return await _tableStorage.GetDataAsync(x => x.AssetPairId == assetPairId);
        }
    }
}