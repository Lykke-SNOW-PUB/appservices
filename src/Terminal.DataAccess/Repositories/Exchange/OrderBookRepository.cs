﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Lykke.Terminal.DataAccess.Mongo;
using Lykke.Terminal.Domain.Exchange;

namespace Lykke.Terminal.DataAccess.Repositories.Exchange
{
    public class OrderBookEntity : MongoEntity, IOrderBook
    {
        public string OrderBookId => BsonId;
        public string AssetPairId { get; set; }
        public bool IsBuy { get; set; }
        public DateTime Timestamp { get; set; }
        public IEnumerable<OrderBookLine> Prices { get; set; }

        public static OrderBookEntity Create(IOrderBook src)
        {
            var orderBookEntity = new OrderBookEntity
            {
                BsonId = Guid.NewGuid().ToString()
            };
            orderBookEntity.Update(src);

            return orderBookEntity;
        }

        internal void Update(IOrderBook src)
        {
            AssetPairId = src.AssetPairId;
            IsBuy = src.IsBuy;
            Timestamp = src.Timestamp;
            Prices = src.Prices;
        }
    }

    public class OrderBookRepository : Repository<IOrderBook, OrderBookEntity>, IOrderBookRepository
    {
        private readonly IMongoStorage<OrderBookEntity> _tableStorage;

        public OrderBookRepository(IMongoStorage<OrderBookEntity> tableStorage) : base(tableStorage)
        {
            _tableStorage = tableStorage;
        }

        public override async Task AddAsync(IOrderBook entity)
        {
            var newEntity = OrderBookEntity.Create(entity);
            await _tableStorage.InsertAsync(newEntity);
        }

        public override Task UpdateAsync(IOrderBook entity)
        {
            return _tableStorage.ReplaceAsync(entity.OrderBookId, itm =>
            {
                itm.Update(entity);
                return itm;
            });
        }

        public async Task<IEnumerable<IOrderBook>> GetByAssetPairIdAndBuy(IOrderBook entity)
        {
            return await _tableStorage.GetDataAsync(o => o.IsBuy == entity.IsBuy && o.AssetPairId == entity.AssetPairId);
        }
    }
}