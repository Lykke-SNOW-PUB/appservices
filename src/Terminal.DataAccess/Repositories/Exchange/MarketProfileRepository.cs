﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Lykke.Terminal.DataAccess.Mongo;
using Lykke.Terminal.Domain.Exchange;
using MongoDB.Bson.Serialization.Attributes;

namespace Lykke.Terminal.DataAccess.Repositories.Exchange
{
    public class AssetPairQuoteEntity : MongoEntity, IAssetPairQuote
    {
        [BsonIgnore]
        public string AssetPairId => BsonId;
        public DateTime DateTime { get; set; }
        public double Bid { get; set; }
        public double Ask { get; set; }

        public static AssetPairQuoteEntity Create(IAssetPairQuote src)
        {
            var result = new AssetPairQuoteEntity
            {
                BsonId = src.AssetPairId
            };

            result.Update(src);

            return result;
        }

        internal void Update(IAssetPairQuote src)
        {
            Ask = src.Ask;
            Bid = src.Bid;
            DateTime = src.DateTime;
        }
    }

    public class MarketProfileRepository : IMarketProfileRepository
    {
        private readonly IMongoStorage<AssetPairQuoteEntity> _tableStorage;

        public MarketProfileRepository(IMongoStorage<AssetPairQuoteEntity> tableStorage)
        {
            _tableStorage = tableStorage;
        }

        public async Task<IEnumerable<IAssetPairQuote>> GetAllAsync()
        {
            return await _tableStorage.GetDataAsync();
        }

        public async Task AddAsync(IAssetPairQuote entity)
        {
            var newEntity = AssetPairQuoteEntity.Create(entity);
            await _tableStorage.InsertAsync(newEntity);
        }

        public Task UpdateAsync(IAssetPairQuote entity)
        {
            return _tableStorage.ReplaceAsync(entity.AssetPairId, itm =>
            {
                itm.Update(entity);
                return itm;
            });
        }

        public Task DeteleAsync(string id)
        {
            throw new NotImplementedException();
        }

        public async Task<IAssetPairQuote> GetByIdAsync(string id)
        {
            return await _tableStorage.GetDataAsync(id);
        }
    }
}