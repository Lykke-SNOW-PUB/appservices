﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Lykke.Terminal.DataAccess.Mongo;
using Lykke.Terminal.Domain.Exchange;
using MongoDB.Bson.Serialization.Attributes;

namespace Lykke.Terminal.DataAccess.Repositories.Exchange
{
    public class DoneOrderEntity : MongoEntity, IDoneOrder
    {
        [BsonIgnore]
        public string Id => BsonId;

        public string ClientId { get; set; }
        public string TradingAccountId { get; set; }
        public DateTime CreatedAt { get; set; }
        public double Volume { get; set; }
        public double Price { get; set; }
        public string AssetPairId { get; set; }
        public OrderStatus Status { get; set; }
        public double DefinedPrice { get; set; }
        public DateTime LastModified { get; set; }
        public OrderComment Comment { get; set; }
        public double TakeProfit { get; set; }
        public double StopLoss { get; set; }
        public double Commission { get; set; }
        public double ProfitLoss { get; set; }

        public static DoneOrderEntity Create(IDoneOrder src)
        {
            return new DoneOrderEntity
            {
                BsonId = src.Id,
                ClientId = src.ClientId,
                TradingAccountId = src.TradingAccountId,
                CreatedAt = src.CreatedAt,
                Volume = src.Volume,
                AssetPairId = src.AssetPairId,
                Status = src.Status,
                LastModified = src.LastModified,
                ProfitLoss = src.ProfitLoss,
                Price = src.Price,
                Comment = src.Comment,
                TakeProfit = src.TakeProfit,
                StopLoss = src.StopLoss,
                Commission = src.Commission
            };
        }
    }

    public class DoneOrdersRepository : Repository<IDoneOrder, DoneOrderEntity>, IDoneOrdersRepository
    {
        private readonly IMongoStorage<DoneOrderEntity> _tableStorage;

        public DoneOrdersRepository(IMongoStorage<DoneOrderEntity> tableStorage) : base(tableStorage)
        {
            _tableStorage = tableStorage;
        }

        public override async Task AddAsync(IDoneOrder entity)
        {
            var newEntity = DoneOrderEntity.Create(entity);
            await _tableStorage.InsertAsync(newEntity);
        }

        public override Task UpdateAsync(IDoneOrder entity)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<IDoneOrder>> GetByClientId(string clientId)
        {
            return await _tableStorage.GetDataAsync(x => x.ClientId == clientId);
        }
    }
}