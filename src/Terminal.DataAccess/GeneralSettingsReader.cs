﻿using Newtonsoft.Json;

namespace Lykke.Terminal.DataAccess
{
    public static class GeneralSettingsReader
    {
        public static T ReadSettingsFromData<T>(string jsonData)
        {
            return JsonConvert.DeserializeObject<T>(jsonData);
        }
    }
}