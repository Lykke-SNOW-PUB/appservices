﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Lykke.Terminal.DataAccess.Utils;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;

namespace Lykke.Terminal.DataAccess.Mongo
{
     public class MongoStorage<T> : IMongoStorage<T> where T : MongoEntity
    {
        private readonly IMongoCollection<T> _collection;

        public MongoStorage(IMongoClient mongoClient, string tableName, string dbName = "CFDPlatformDb")
        {
            var db = mongoClient.GetDatabase(dbName);
            _collection = db.GetCollection<T>(tableName);
        }

        public async Task<IEnumerable<T>> GetDataAsync()
        {
            return await _collection.Find(FilterDefinition<T>.Empty).ToListAsync();
        }

        public async Task<T> GetDataAsync(string key)
        {
            return await _collection.Find(x => x.BsonId == key).FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<T>> GetDataAsync(Expression<Func<T, bool>> filter)
        {
            return await _collection.Find(filter).ToListAsync();
        }

        public async Task<T> InsertOrReplaceAsync(T item)
        {
            while (true)
            {
                try
                {
                    var entity = await GetDataAsync(item.BsonId);

                    if (entity == null)
                    {
                        await _collection.InsertOneAsync(item);
                        return item;
                    }

                    item.BsonVersion = entity.BsonVersion + 1;

                    var output = await _collection.ReplaceOneAsync(x => x.BsonId == item.BsonId && x.BsonVersion == entity.BsonVersion, item);
                    if (output.ModifiedCount > 0)
                        return item;
                }
                catch (MongoWriteException ex)
                {
                    if (!ex.IsDuplicateError())
                        throw;
                }
            }
        }

        public Task DeleteAsync(Expression<Func<T, bool>> filter)
        {
            return _collection.DeleteManyAsync(filter);
        }

        public async Task<T> InsertOrModifyAsync(string key, Func<T> createNew, Func<T, T> modify)
        {
            while (true)
            {
                try
                {
                    var entity = await GetDataAsync(key);

                    T item;

                    if (entity == null)
                    {
                        item = createNew();
                        await _collection.InsertOneAsync(item);
                        return item;
                    }
                    var prevVersion = entity.BsonVersion;
                    item = modify(entity);
                    item.BsonVersion = prevVersion + 1;

                    var output = await _collection.ReplaceOneAsync(x => x.BsonId == item.BsonId && x.BsonVersion == prevVersion, item);
                    if (output.ModifiedCount > 0)
                        return item;
                }
                catch (MongoWriteException ex)
                {
                    if (!ex.IsDuplicateError())
                        throw;
                }
            }
        }

        public Task GetDataByChunksAsync(Action<IEnumerable<T>> action)
        {
            return ExecuteQueryAsync(null, itms =>
            {
                action(itms);
                return true;
            });
        }

        public Task InsertAsync(T item)
        {
            return _collection.InsertOneAsync(item);
        }

        public Task DeleteAsync(T record)
        {
            if (record != null)
                return DeleteAsync(record.BsonId);
            return Task.CompletedTask;
        }

        public async Task InsertOrMergeAsync(T entity)
        {
            while (true)
            {
                try
                {
                    var record = await GetDataAsync(entity.BsonId);

                    if (record != null)
                    {
                        var prevVersion = record.BsonVersion;
                        var currentDoc = record.ToBsonDocument();
                        var merged = currentDoc.MergeExt(entity.ToBsonDocument());
                        var replace = BsonSerializer.Deserialize<T>(merged);
                        replace.BsonVersion = prevVersion + 1;
                        var output = await _collection.ReplaceOneAsync(x => x.BsonId == record.BsonId && x.BsonVersion == prevVersion, replace);
                        if (output.ModifiedCount > 0)
                            return;
                    }
                    else
                    {
                        await _collection.InsertOneAsync(entity);
                        return;
                    }

                }
                catch (MongoWriteException ex)
                {
                    if (!ex.IsDuplicateError())
                        throw;
                }
            }
        }

        public Task<T> GetTopRecordAsync(Expression<Func<T, bool>> filter, Expression<Func<T, object>> sortSelector, SortDirection direction)
        {
            var query = _collection.Find(filter);
            if (direction == SortDirection.Ascending)
                query = query.SortBy(sortSelector);
            else
                query = query.SortByDescending(sortSelector);
            return query.FirstOrDefaultAsync();
        }

        public Task InsertAsync(IEnumerable<T> documents)
        {
            return _collection.InsertManyAsync(documents);
        }

        public Task ScanDataAsync(Func<IEnumerable<T>, Task> chunk)
        {
            return ExecuteQueryAsync(null, chunk);
        }

        public Task InsertOrReplaceBatchAsync(T[] entities)
        {
            return _collection.BulkWriteAsync(entities.Select(d =>
                new ReplaceOneModel<T>(new FilterDefinitionBuilder<T>().Eq(x => x.BsonId, d.BsonId), d)
                {
                    IsUpsert = true
                }));
        }

        public async Task<T> InsertAndGenerateRowKeyAsDateTimeAsync(T entity, DateTime dt, bool shortFormat = false)
        {
            int n = 0;
            var id = entity.BsonId;
            while (true)
            {
                try
                {
                    var suffix = dt + (shortFormat ? n.ToString("000") : '.' + n.ToString("000"));
                    entity.BsonId = id
                        + "_" + suffix;
                    await InsertAsync(entity);
                    return entity;
                }
                catch (MongoWriteException ex)
                {
                    if (!ex.IsDuplicateError())
                        throw;
                }
                n++;
                if (n == 999)
                    throw new Exception("Can't insert item");
            }
        }

        public async Task<T> FirstOrNullViaScanAsync(Func<IEnumerable<T>, T> dataToSearch)
        {
            T result = null;

            await ExecuteQueryAsync(null,
                itms =>
                {
                    result = dataToSearch(itms);
                    return result == null;
                });

            return result;
        }

        public async Task<T> InsertAndGenerateRowKeyAsTimeAsync(T newEntity, DateTime dateTime)
        {
            int n = 0;
            var dt = dateTime.ToString("HH:mm:ss");
            while (true)
            {
                try
                {
                    newEntity.BsonId = dt + '.' + n.ToString("000");
                    await InsertAsync(newEntity);
                    return newEntity;
                }
                catch (MongoWriteException ex)
                {
                    if (!ex.IsDuplicateError())
                        throw;
                }
                n++;
                if (n == 999)
                    throw new Exception("Can't insert item");
            }
        }


        public async Task<T> MergeAsync(string key, Func<T, T> func)
        {
            while (true)
            {
                var entity = await GetDataAsync(key);
                if (entity == null) return null;

                var prevVersion = entity.BsonVersion;

                var currentDoc = entity.ToBsonDocument();

                var newDoc = func(entity).ToBsonDocument();

                var merged = currentDoc.MergeExt(newDoc);

                var item = BsonSerializer.Deserialize<T>(merged);
                item.BsonVersion = prevVersion + 1;
                var output = await _collection.ReplaceOneAsync(x => x.BsonId == entity.BsonId && x.BsonVersion == prevVersion, item);
                if (output.ModifiedCount > 0)
                    return item;
            }
        }

        public async Task<T> ReplaceAsync(string key, Func<T, T> replaceAction)
        {
            while (true)
            {
                var entity = await GetDataAsync(key);
                if (entity == null)
                    return null;

                var prevVersion = entity.BsonVersion;

                var result = replaceAction(entity);
                if (result == null)
                    return null;

                result.BsonVersion = prevVersion + 1;
                var output = await _collection.ReplaceOneAsync(x => x.BsonId == key && x.BsonVersion == prevVersion, result);
                if (output.ModifiedCount > 0)
                    return result;
            }
        }

        public Task DeleteAsync(string id)
        {
            return _collection.DeleteOneAsync(o => o.BsonId == id);
        }

        public void DeleteAll()
        {
            _collection.DeleteMany(o => true);
        }

        private async Task ExecuteQueryAsync(Func<T, bool> filter, Func<IEnumerable<T>, bool> yieldData)
        {
            int skip = 0;
            int limit = 10;
            do
            {
                var queryResponse = await _collection.Find(FilterDefinition<T>.Empty).Skip(skip).Limit(limit).ToListAsync();
                var shouldWeContinue = yieldData(filter != null ? queryResponse.Where(filter) : queryResponse);
                if (!shouldWeContinue)
                    break;
                if (queryResponse.Count == 0)
                    return;
                skip += limit;
            }
            while (true);
        }

        private async Task ExecuteQueryAsync(Func<T, bool> filter, Func<IEnumerable<T>, Task> yieldData)
        {
            int skip = 0;
            int limit = 10;
            do
            {
                var queryResponse = await _collection.Find(FilterDefinition<T>.Empty).Skip(skip).Limit(limit).ToListAsync();
                await yieldData(filter != null ? queryResponse.Where(filter) : queryResponse);
                if (queryResponse.Count == 0)
                    return;
                skip += limit;
            }
            while (true);
        }

    }
}
