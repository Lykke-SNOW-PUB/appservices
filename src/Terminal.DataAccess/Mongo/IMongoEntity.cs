﻿using MongoDB.Bson.Serialization.Attributes;

namespace Lykke.Terminal.DataAccess.Mongo
{	
	public abstract class MongoEntity
	{
		[BsonId]
		public string BsonId { get; set; }

		public int BsonVersion { get; set; }
	}
}
