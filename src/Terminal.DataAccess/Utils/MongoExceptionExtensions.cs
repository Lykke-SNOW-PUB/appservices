﻿using MongoDB.Driver;

namespace Lykke.Terminal.DataAccess.Utils
{
	public static class MongoExceptionExtensions
	{
		public static bool IsDuplicateError(this MongoWriteException ex)
		{
			return ex.Message.Contains("duplicate key error");
		}
	}
}
