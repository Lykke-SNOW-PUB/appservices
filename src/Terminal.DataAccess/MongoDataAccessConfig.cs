﻿using Lykke.Terminal.DataAccess.Mongo;
using Lykke.Terminal.DataAccess.Repositories.Accounts;
using Lykke.Terminal.DataAccess.Repositories.Exchange;
using Lykke.Terminal.DataAccess.Repositories.Log;
using Lykke.Terminal.Domain.Accounts;
using Lykke.Terminal.Domain.Exchange;
using Lykke.Terminal.Domain.Log;
using Lykke.Terminal.Domain.Settings;
using MongoDB.Driver;
using StructureMap;

namespace Lykke.Terminal.DataAccess
{
    public class MongoDataAccessConfig : Registry
    {
        public MongoDataAccessConfig(IBaseSettings settings)
        {
            var log = CreateLogToTable(settings.Db.LogsConnString);
            For<ILog>().Add(log);

            BindTradingRepos(settings.Db);

            BindLiquidityHistory(settings.Db);

            BindAccoutInfo(settings.Db);

            BindDicts(settings.Db);
        }

        private void BindLiquidityHistory(DbSettings dbSetting)
        {
            var liquidityMongoClient = new MongoClient(dbSetting.HLiquidityConnString);

            For<IMarketProfileRepository>()
                .Add(new MarketProfileRepository(new MongoStorage<AssetPairQuoteEntity>(liquidityMongoClient, "MarketProfile")));
        }

        private void BindDicts(DbSettings dbSetting)
        {
            var dictMongoClient = new MongoClient(dbSetting.DictsConnString);
        }

        private void BindAccoutInfo(DbSettings dbSetting)
        {
            var clientMongoClient = new MongoClient(dbSetting.ClientPersonalInfoConnString);

            For<IAccountInfoRepository>()
                .Add(new AccountInfoRepository(new MongoStorage<AccountInfoEntity>(clientMongoClient, "AccountInfo")));

            For<ITradingAccountRepository>()
                .Add(new TradingAccountRepository(new MongoStorage<TradingAccountEntity>(clientMongoClient, "TradingAccount")));
        }

        public static LogToTable CreateLogToTable(string connString)
        {
            return new LogToTable(new MongoStorage<LogEntity>(new MongoClient(connString), "LogTerminalApplication"));
        }

        private void BindTradingRepos(DbSettings dbSettings)
        {
            var htradesMongoClient = new MongoClient(dbSettings.HTradesConnString);
            var alimitsMongoClient = new MongoClient(dbSettings.ALimitOrdersConnString);
            var hlimitsMongoCLient = new MongoClient(dbSettings.HLimitOrdersConnString);
            var hmarketMongoClient = new MongoClient(dbSettings.HMarketOrdersConnString);

            For<IPendingOrdersRepository>()
                .Add(
                    new PendingOrdersRepository(new MongoStorage<PendingOrderEntity>(alimitsMongoClient, "PendingOrders")));

            For<IActiveOrdersRepository>()
                .Add(new ActiveOrdersRepository(new MongoStorage<ActiveOrderEntity>(hmarketMongoClient, "ActiveOrders")));

            For<IDoneOrdersRepository>()
                .Add(
                    new DoneOrdersRepository(new MongoStorage<DoneOrderEntity>(hlimitsMongoCLient,
                        "DoneOrders")));

            For<IOrderBookRepository>()
                .Add(new OrderBookRepository(new MongoStorage<OrderBookEntity>(hmarketMongoClient, "OrderBook")));
        }
    }
}