﻿using System;
using Lykke.Terminal.Domain.Log;

namespace Lykke.Terminal.Common.Validation
{
    public class GeneralSettingsValidator
    {
        public static void Validate<T>(T settings, ILog log = null)
        {
            try
            {
                ValidationHelper.ValidateObjectRecursive(settings);
            }
            catch (Exception e)
            {
                log?.WriteFatalErrorAsync("GeneralSettings", "Validation", null, e);

                throw;
            }
        }
    }
}