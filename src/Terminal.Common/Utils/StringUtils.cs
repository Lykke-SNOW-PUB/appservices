﻿using System;

namespace Lykke.Terminal.Common.Utils
{
    public class StringUtils
    {
        public static class IdGenerator
        {
            public static string GenerateDateTimeId(DateTime creationDateTime)
            {
                return $"{creationDateTime:yyyyMMddHHmmssfff}_{Guid.NewGuid():N}";
            }

            public static string GenerateDateTimeIdNewFirst(DateTime creationDateTime)
            {
                return $"{DateTime.MaxValue.Ticks - creationDateTime.Ticks:d19}_{Guid.NewGuid():N}";
            }
        }
    }
}