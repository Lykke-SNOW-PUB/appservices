﻿using StructureMap;

namespace Lykke.Terminal.Common.Utils
{
    public class DependencyContainer
    {
        public static Container Instance { get; }

        static DependencyContainer()
        {
            Instance = new Container();
        }
    }
}