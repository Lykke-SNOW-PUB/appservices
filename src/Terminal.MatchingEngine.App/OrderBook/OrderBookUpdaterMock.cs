﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Lykke.Terminal.Domain.Assets;
using Lykke.Terminal.Domain.Dictionaries;
using Lykke.Terminal.Domain.Exchange;
using Lykke.Terminal.Domain.Messaging.Dtos;
using Lykke.Terminal.Domain.Messaging.Handlers;
using Lykke.Terminal.Messaging;

namespace Lykke.Terminal.MatchingEngine.App.OrderBook
{
    public class OrderBookUpdaterMock : IOrderBookUpdater
    {
        private readonly IAssetPairDictionaryService _assetPairDictionaryService;
        private readonly IBrokerMessageHandler<OrderBookMessageDto> _brokerMessageHandler;
        private readonly IMarketProfileService _marketProfileService;
        private readonly IOrderBookRepository _orderBookRepository;
        private readonly Random _rnd = new Random();
        private readonly object _sync = new object();
        private Timer _stateTimer;

        public OrderBookUpdaterMock(IOrderBookRepository orderBookRepository, IMarketProfileService marketProfileService,
            IAssetPairDictionaryService assetPairDictionaryService1,
            IBrokerMessageHandler<OrderBookMessageDto> brokerMessageHandler)
        {
            _orderBookRepository = orderBookRepository;
            _marketProfileService = marketProfileService;
            _assetPairDictionaryService = assetPairDictionaryService1;
            _brokerMessageHandler = brokerMessageHandler;
        }

        public void Update(BrokerConfiguration meBrokerConfiguration)
        {
            _stateTimer = new Timer(OrderBookUpdateRunner, null, 0, Timeout.Infinite);
        }

        public void OrderBookUpdateRunner(object state)
        {
            lock (_sync)
            {
                UpdateOrderBookAsync().Wait();
            }
        }

        private async Task UpdateOrderBookAsync()
        {
            var orderBook = await _orderBookRepository.GetAllAsync();

            if ((orderBook == null) || !orderBook.Any())
            {
                await InitializeOrderBookAsync();
            }
            else
            {
                var assetPairs = await _assetPairDictionaryService.GetAllAsync();
                var pairs = assetPairs as IList<IAssetPair> ?? assetPairs.ToList();
                var assetId = _rnd.Next(pairs.Count);

                var assetPair = pairs[assetId];

                var orderBookUpdated = UpdateOrderBookForAssetPair(assetPair);

                await _brokerMessageHandler.HandleAsync(Mapper.Map<OrderBookMessageDto>(orderBookUpdated.Item1), null);

                await _brokerMessageHandler.HandleAsync(Mapper.Map<OrderBookMessageDto>(orderBookUpdated.Item2), null);
            }

            _stateTimer.Change(50, Timeout.Infinite);
        }

        private async Task InitializeOrderBookAsync()
        {
            var assetPairs = await _assetPairDictionaryService.GetAllAsync();

            foreach (var assetPair in assetPairs)
            {
                var updatedOrderBook = UpdateOrderBookForAssetPair(assetPair);

                await _orderBookRepository.AddAsync(updatedOrderBook.Item1);
                await _marketProfileService.UpdateMarketProfileAsync(updatedOrderBook.Item1);

                await _orderBookRepository.AddAsync(updatedOrderBook.Item2);
                await _marketProfileService.UpdateMarketProfileAsync(updatedOrderBook.Item2);
            }
        }

        private Tuple<Domain.Exchange.OrderBook, Domain.Exchange.OrderBook> UpdateOrderBookForAssetPair(
            IAssetPair assetPair)
        {
            var orderBookLevel = _rnd.Next(1, 15);

            var orderBookSellLines = new List<OrderBookLine>();
            var orderBookBuyLines = new List<OrderBookLine>();

            for (var i = 0; i < orderBookLevel; i++)
            {
                var ask = _rnd.NextDouble()*(1000 - 100) + 100;
                var askVolume = _rnd.Next(1000, 100000);
                orderBookSellLines.Add(new OrderBookLine
                {
                    Price = ask,
                    Volume = askVolume
                });

                var bid = ask - (_rnd.NextDouble()*(3 - 0.1) + 0.1);
                var bidVolume = _rnd.Next(1000, 100000);
                orderBookBuyLines.Add(new OrderBookLine
                {
                    Price = bid,
                    Volume = bidVolume
                });
            }

            var orderSell = new Domain.Exchange.OrderBook
            {
                AssetPairId = assetPair.Id,
                IsBuy = false,
                Timestamp = DateTime.Now,
                Prices = orderBookSellLines
            };

            var orderBuy = new Domain.Exchange.OrderBook
            {
                AssetPairId = assetPair.Id,
                IsBuy = true,
                Timestamp = DateTime.Now,
                Prices = orderBookBuyLines
            };

            return new Tuple<Domain.Exchange.OrderBook, Domain.Exchange.OrderBook>(orderSell, orderBuy);
        }
    }
}