﻿using Lykke.Terminal.Messaging;

namespace Lykke.Terminal.MatchingEngine.App.OrderBook
{
    public interface IOrderBookUpdater
    {
        void Update(BrokerConfiguration meBrokerConfiguration);
    }
}