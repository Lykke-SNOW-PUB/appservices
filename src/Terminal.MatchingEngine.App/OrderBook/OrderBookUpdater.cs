﻿using System.Threading.Tasks;
using Lykke.Terminal.Domain.Messaging;
using Lykke.Terminal.Domain.Messaging.Dtos;
using Lykke.Terminal.Domain.Messaging.Handlers;
using Lykke.Terminal.Domain.Settings;
using Lykke.Terminal.Messaging;

namespace Lykke.Terminal.MatchingEngine.App.OrderBook
{
    public class OrderBookUpdater : IOrderBookUpdater
    {
        private readonly IBaseSettings _baseSettings;
        private readonly IBrokerMessageHandler<OrderBookMessageDto> _brokerMessageHandler;

        public OrderBookUpdater(IBrokerMessageHandler<OrderBookMessageDto> brokerMessageHandler,
            IBaseSettings baseSettings)
        {
            _brokerMessageHandler = brokerMessageHandler;
            _baseSettings = baseSettings;
        }

        public void Update(BrokerConfiguration meBrokerConfiguration)
        {
            Task.Run(async () => await ReceiveOrderBookAsync(meBrokerConfiguration));
        }

        private async Task ReceiveOrderBookAsync(BrokerConfiguration settingsBrokerConfiguration)
        {
            var connectionFactory = BrokerConnectionFactory.Create(settingsBrokerConfiguration);
            var broker = connectionFactory.GetBroker();

            await
                broker.SubscribeAsync<OrderBookMessageDto>(_baseSettings.MatchingEngineSettings.QuotesQueueName, null,
                    _brokerMessageHandler.HandleAsync, BrokerExchangeType.Fanout);
        }
    }
}