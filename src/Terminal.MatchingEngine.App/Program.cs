﻿using System;
using System.IO;
using AutoMapper;
using Lykke.Terminal.BusinessService;
using Lykke.Terminal.Common.Utils;
using Lykke.Terminal.Common.Validation;
using Lykke.Terminal.DataAccess;
using Lykke.Terminal.Domain.Settings;
using Lykke.Terminal.MatchingEngine.App.OrderBook;
using Lykke.Terminal.Messaging;
using Microsoft.Extensions.Configuration;

namespace Lykke.Terminal.MatchingEngine.App
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var config = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .Build();

            var rawRabbitConfig = config.GetSection("WriteRawRabbit");
            var brokerConfiguration = rawRabbitConfig.Bind<BrokerConfiguration>();

            var settings = ReadGeneralSettings();

            var quotesQueueName = config.GetValue<string>("QuotesQueueName");
            if (string.IsNullOrEmpty(quotesQueueName))
            {
                throw new Exception("Please, provide QuotesQueueName");
            }

            settings.MatchingEngineSettings = new Domain.Settings.MatchingEngine {QuotesQueueName = quotesQueueName};

            Mapper.Initialize(cfg => cfg.AddProfile<AutoMapperConfig>());

            var container = DependencyContainer.Instance;

            container.Configure(_ =>
            {
                _.AddRegistry(new MongoDataAccessConfig(settings));
                _.AddRegistry(new BusinessServiceConfig(brokerConfiguration));
                _.For<IOrderBookUpdater>().Add<OrderBookUpdater>();
                _.For<IBaseSettings>().Add(settings);
            });

            var orderBookUpdater = container.GetInstance<IOrderBookUpdater>();

            var meRawRabbitConfig = config.GetSection("MatchingEngineReadRawRabbit");
            var meBrokerConfiguration = meRawRabbitConfig.Bind<BrokerConfiguration>();
            orderBookUpdater.Update(meBrokerConfiguration);

            Console.ReadLine();
        }

        private static IBaseSettings ReadGeneralSettings()
        {
            var settingsData = ReadSettingsFile();

            if (string.IsNullOrWhiteSpace(settingsData))
            {
                throw new Exception("Please, provide mongo_generalsettings.json file");
            }

            var settings = GeneralSettingsReader.ReadSettingsFromData<BaseSettings>(settingsData);

            GeneralSettingsValidator.Validate(settings);

            return settings;
        }

        private static string ReadSettingsFile()
        {
            try
            {
#if DEBUG
                return File.ReadAllText(string.Format("..{0}..{0}..{0}settings{0}mongo_generalsettings.json", Path.DirectorySeparatorChar));
#else
                return File.ReadAllText("mongo_generalsettings.json");
#endif
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}