﻿using System;
using AutoMapper;
using Lykke.Terminal.BusinessService.EventFilters;
using Lykke.Terminal.Domain.Settings;
using Lykke.Terminal.MatchingEngine.Configurations;
using Lykke.Terminal.Messaging;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using RawRabbit.Extensions.Client;

namespace Lykke.Terminal.MatchingEngine
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", true, true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", true);

            builder.AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            var provider = services.BuildServiceProvider();
            var settings = provider.GetService<IBaseSettings>();

            //todo: move all configs to 1 file
            var tradeServerConfig = new TradeServerSettings
            {
                Host = Configuration.GetValue<string>("TradeService:Host"),
                Port = Configuration.GetValue<int>("TradeService:Port")
            };
            settings.TradeServerSettings = tradeServerConfig;

            var settingsConfig = new SettingsConfig
            {
                BaseSettings = settings,
                BrokerConfiguration = provider.GetService<IOptions<BrokerConfiguration>>().Value
            };

            services.AddRawRabbit(Configuration.GetSection("RawRabbit"));

            services.AddCors();

            services
                .AddMvc()
                .AddMvcOptions(o => { o.Filters.Add(typeof(UnhandledExceptionFilter)); });

            services.AddAutoMapper();

            return ApiDependencies.Create(services, settingsConfig);
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            app.UseCors(builder =>
            {
                //todo: read from config or remove
                builder
                    .AllowAnyOrigin()
                    .AllowAnyHeader()
                    .AllowAnyMethod();
            });

            app.UseMvcWithDefaultRoute();
        }
    }
}