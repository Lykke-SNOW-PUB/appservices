﻿using System.Threading.Tasks;
using Lykke.Terminal.Domain.ApplicationServices.Trading;
using Microsoft.AspNetCore.Mvc;

namespace Lykke.Terminal.MatchingEngine.Controllers
{
    [Route("api/[controller]")]
    public class OrderController : Controller
    {
        private readonly ITradingAppService _tradingService;

        public OrderController(ITradingAppService tradingService)
        {
            _tradingService = tradingService;
        }

        [HttpPost]
        public async Task<IActionResult> OpenOrder([FromBody] OpenOrderContext context)
        {
            await _tradingService.OpenOrderAsync(context);
            return Ok();
        }

        [HttpDelete]
        public async Task CloseOrder([FromQuery] string accountId, [FromQuery] string orderId)
        {
            await _tradingService.CloseOrderAsync(accountId, orderId);
        }
    }
}