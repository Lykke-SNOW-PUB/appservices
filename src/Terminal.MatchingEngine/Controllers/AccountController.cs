﻿using System.Threading.Tasks;
using Lykke.Terminal.Domain.Accounts;
using Lykke.Terminal.Domain.ApplicationServices.Accounts;
using Microsoft.AspNetCore.Mvc;

namespace Lykke.Terminal.MatchingEngine.Controllers
{
    [Route("api/[controller]")]
    public class AccountController : Controller
    {
        private readonly IAccountAppService _accountService;

        public AccountController(IAccountAppService accountService)
        {
            _accountService = accountService;
        }

        [HttpPatch]
        public async Task UpdateAsync(AccountInfo context)
        {
            await _accountService.UpdateAccountInfoAsync(context);
        }
    }
}