﻿using Microsoft.AspNetCore.Mvc;

namespace Lykke.Terminal.MatchingEngine.Controllers
{
    [Route("/api/whoami")]
    public class WhoamiController : Controller
    {
        public IActionResult Get()
        {
            return Content("Matching Engine Web Api");
        }
    }
}