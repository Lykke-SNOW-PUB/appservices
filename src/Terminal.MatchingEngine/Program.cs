﻿using System;
using System.IO;
using Lykke.Terminal.Common.Validation;
using Lykke.Terminal.DataAccess;
using Lykke.Terminal.Domain.Settings;
using Lykke.Terminal.Messaging;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Lykke.Terminal.MatchingEngine
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var config = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddEnvironmentVariables()
                .AddJsonFile("appsettings.json", true)
                .Build();

            var settings = ReadGeneralSettings();

            var host = new WebHostBuilder()
                .UseKestrel()
                .UseContentRoot(Directory.GetCurrentDirectory())
                .UseIISIntegration()
                .ConfigureServices(
                    collection =>
                        collection.AddSingleton(settings)
                            .AddOptions()
                            .Configure<BrokerConfiguration>(config.GetSection("RawRabbit")))
                .UseStartup<Startup>()
                .Build();

            host.Run();
        }

        //TODO: if settings are the same for all apps merge methods
        private static IBaseSettings ReadGeneralSettings()
        {
            var settingsData = ReadSettingsFile();

            if (string.IsNullOrWhiteSpace(settingsData))
            {
                throw new Exception("Please, provide mongo_generalsettings.json file");
            }

            var settings = GeneralSettingsReader.ReadSettingsFromData<BaseSettings>(settingsData);

            GeneralSettingsValidator.Validate(settings);

            return settings;
        }

        private static string ReadSettingsFile()
        {
            try
            {
#if DEBUG
                return File.ReadAllText(string.Format("..{0}..{0}..{0}settings{0}mongo_generalsettings.json", Path.DirectorySeparatorChar));
#else
                return File.ReadAllText("mongo_generalsettings.json");
#endif
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}