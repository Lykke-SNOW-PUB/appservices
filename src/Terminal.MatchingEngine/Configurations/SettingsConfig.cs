﻿using Lykke.Terminal.Domain.Messaging;
using Lykke.Terminal.Domain.Settings;

namespace Lykke.Terminal.MatchingEngine.Configurations
{
    public class SettingsConfig
    {
        public IBrokerConfiguration BrokerConfiguration { get; set; }
        public IBaseSettings BaseSettings { get; set; }
    }
}