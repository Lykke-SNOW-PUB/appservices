﻿using System;
using Lykke.Terminal.BusinessService;
using Lykke.Terminal.Common.Utils;
using Lykke.Terminal.DataAccess;
using Microsoft.Extensions.DependencyInjection;
using StructureMap;

namespace Lykke.Terminal.MatchingEngine.Configurations
{
    public static class ApiDependencies
    {
        public static IServiceProvider Create(IServiceCollection services, SettingsConfig settings)
        {
            var container = DependencyContainer.Instance;

            container.Configure(
                _ =>
                {
                    _.AddRegistry(new MongoDataAccessConfig(settings.BaseSettings));
                    _.AddRegistry(new BusinessServiceConfig(settings.BrokerConfiguration));
                });

            container.Populate(services);

            return container.GetInstance<IServiceProvider>();
        }

    }
}