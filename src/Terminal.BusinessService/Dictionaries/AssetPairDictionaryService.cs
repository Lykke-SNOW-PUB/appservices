﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Lykke.Terminal.BusinessService.Infrastructure;
using Lykke.Terminal.Domain.Assets;
using Lykke.Terminal.Domain.Dictionaries;

namespace Lykke.Terminal.BusinessService.Dictionaries
{
    public class AssetPairDictionaryService : IAssetPairDictionaryService, IApplicationService
    {
        public Task<IEnumerable<IAssetPair>> GetAllAsync()
        {
            return Task.FromResult(GenerateAssetPairs());
        }

        public Task<IAssetPair> GetByBaseAndQuotingIds(string baseAssetId, string quotingAssetId)
        {
            return
                Task.FromResult(
                    GenerateAssetPairs()
                        .FirstOrDefault(x => (x.BaseAssetId == baseAssetId) && (x.QuotingAssetId == quotingAssetId)));
        }

        public Task<IAssetPair> GetByIdAsync(string id)
        {
            return Task.FromResult(GenerateAssetPairs().FirstOrDefault(x => x.Id == id));
        }

        private IEnumerable<IAssetPair> GenerateAssetPairs()
        {
            IEnumerable<AssetPair> assetPairs = new List<AssetPair>
            {
                new AssetPair
                {
                    Id = "DE000PB0BC06",
                    Name = "DE000PB0BC06",
                    BaseAssetId = "BTC",
                    QuotingAssetId = "EUR",
                    Accuracy = 0,
                    InvertedAccuracy = 0
                },
                new AssetPair
                {
                    Id = "DE000PB8CZG2",
                    Name = "DE000PB8CZG2",
                    BaseAssetId = "BTC",
                    QuotingAssetId = "EUR",
                    Accuracy = 0,
                    InvertedAccuracy = 0
                },
                new AssetPair
                {
                    Id = "DE000PB0S466",
                    Name = "DE000PB0S466",
                    BaseAssetId = "BTC",
                    QuotingAssetId = "EUR",
                    Accuracy = 0,
                    InvertedAccuracy = 0
                },
                new AssetPair
                {
                    Id = "DE000PB5FBT5",
                    Name = "DE000PB5FBT5",
                    BaseAssetId = "BTC",
                    QuotingAssetId = "EUR",
                    Accuracy = 0,
                    InvertedAccuracy = 0
                },
                new AssetPair
                {
                    Id = "DE000PB1U007",
                    Name = "DE000PB1U007",
                    BaseAssetId = "BTC",
                    QuotingAssetId = "EUR",
                    Accuracy = 0,
                    InvertedAccuracy = 0
                },
                new AssetPair
                {
                    Id = "DE000PB7PHQ3",
                    Name = "DE000PB7PHQ3",
                    BaseAssetId = "BTC",
                    QuotingAssetId = "EUR",
                    Accuracy = 0,
                    InvertedAccuracy = 0
                },
                new AssetPair
                {
                    Id = "DE000PR01X95",
                    Name = "DE000PR01X95",
                    BaseAssetId = "BTC",
                    QuotingAssetId = "EUR",
                    Accuracy = 0,
                    InvertedAccuracy = 0
                },
                new AssetPair
                {
                    Id = "DE000PB6HSE5",
                    Name = "DE000PB6HSE5",
                    BaseAssetId = "BTC",
                    QuotingAssetId = "EUR",
                    Accuracy = 0,
                    InvertedAccuracy = 0
                },
                new AssetPair
                {
                    Id = "DE000PB76TM6",
                    Name = "DE000PB76TM6",
                    BaseAssetId = "BTC",
                    QuotingAssetId = "EUR",
                    Accuracy = 0,
                    InvertedAccuracy = 0
                },
                new AssetPair
                {
                    Id = "DE000PA19GS2",
                    Name = "DE000PA19GS2",
                    BaseAssetId = "BTC",
                    QuotingAssetId = "EUR",
                    Accuracy = 0,
                    InvertedAccuracy = 0
                },
                new AssetPair
                {
                    Id = "DE000BN0YW00",
                    Name = "DE000BN0YW00",
                    BaseAssetId = "BTC",
                    QuotingAssetId = "EUR",
                    Accuracy = 0,
                    InvertedAccuracy = 0
                },
                new AssetPair
                {
                    Id = "DE000PB81N72",
                    Name = "DE000PB81N72",
                    BaseAssetId = "BTC",
                    QuotingAssetId = "EUR",
                    Accuracy = 0,
                    InvertedAccuracy = 0
                },
                new AssetPair
                {
                    Id = "DE000PB4M4F3",
                    Name = "DE000PB4M4F3",
                    BaseAssetId = "BTC",
                    QuotingAssetId = "EUR",
                    Accuracy = 0,
                    InvertedAccuracy = 0
                },
                new AssetPair
                {
                    Id = "DE000PS9F7L2",
                    Name = "DE000PS9F7L2",
                    BaseAssetId = "BTC",
                    QuotingAssetId = "EUR",
                    Accuracy = 0,
                    InvertedAccuracy = 0
                },
                new AssetPair
                {
                    Id = "DE000PS8KGE1",
                    Name = "DE000PS8KGE1",
                    BaseAssetId = "BTC",
                    QuotingAssetId = "EUR",
                    Accuracy = 0,
                    InvertedAccuracy = 0
                },
                new AssetPair
                {
                    Id = "DE000PB0BZQ0",
                    Name = "DE000PB0BZQ0",
                    BaseAssetId = "BTC",
                    QuotingAssetId = "EUR",
                    Accuracy = 0,
                    InvertedAccuracy = 0
                },
                new AssetPair
                {
                    Id = "DE000PB0AXF0",
                    Name = "DE000PB0AXF0",
                    BaseAssetId = "BTC",
                    QuotingAssetId = "EUR",
                    Accuracy = 0,
                    InvertedAccuracy = 0
                },
                new AssetPair
                {
                    Id = "DE000PB8Q224",
                    Name = "DE000PB8Q224",
                    BaseAssetId = "BTC",
                    QuotingAssetId = "EUR",
                    Accuracy = 0,
                    InvertedAccuracy = 0
                },
                new AssetPair
                {
                    Id = "DE000PB9MB05",
                    Name = "DE000PB9MB05",
                    BaseAssetId = "BTC",
                    QuotingAssetId = "EUR",
                    Accuracy = 0,
                    InvertedAccuracy = 0
                },
                new AssetPair
                {
                    Id = "DE000PB9MR56",
                    Name = "DE000PB9MR56",
                    BaseAssetId = "BTC",
                    QuotingAssetId = "EUR",
                    Accuracy = 0,
                    InvertedAccuracy = 0
                }
            };

            return assetPairs;
        }
    }
}