﻿using Lykke.Terminal.BusinessService.Infrastructure;
using Lykke.Terminal.BusinessService.Messaging.MessageHandlers;
using Lykke.Terminal.Domain.Messaging;
using Lykke.Terminal.Domain.Messaging.Dtos;
using Lykke.Terminal.Domain.Messaging.Handlers;
using Lykke.Terminal.Messaging;
using StructureMap;

namespace Lykke.Terminal.BusinessService
{
    public class BusinessServiceConfig : Registry
    {
        public BusinessServiceConfig(IBrokerConfiguration settingsBrokerConfiguration)
        {
            Scan(_ =>
            {
                // Declare which assemblies to scan
                _.AssemblyContainingType<IApplicationService>();

                // Built in registration conventions
                _.WithDefaultConventions();
            });

            For<IBrokerMessageHandler<OrderBookMessageDto>>().Use<OrderBookMessageHandler>();

            if (settingsBrokerConfiguration != null)
            {
                IncludeRegistry(new MessagingConfig(settingsBrokerConfiguration));
            }
        }
    }
}