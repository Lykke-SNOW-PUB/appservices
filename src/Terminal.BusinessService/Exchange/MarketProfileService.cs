﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Lykke.Terminal.BusinessService.Infrastructure;
using Lykke.Terminal.Domain.Accounts;
using Lykke.Terminal.Domain.Dictionaries;
using Lykke.Terminal.Domain.Exchange;
using Lykke.Terminal.Domain.Notifications;

namespace Lykke.Terminal.BusinessService.Exchange
{
    public class MarketProfileService : IMarketProfileService, IApplicationService
    {
        private readonly IActiveOrdersRepository _activeOrdersRepository;
        private readonly ITradingAccountService _tradingAccountService;
        private readonly IAssetPairDictionaryService _assetPairDictionaryService;
        private readonly IMarketProfileRepository _marketProfileRepository;
        private readonly IMatchingEngineEvents _matchingEngineEvents;

        public MarketProfileService(IAssetPairDictionaryService assetPairDictionaryService,
            IMatchingEngineEvents matchingEngineEvents, IMarketProfileRepository marketProfileRepository,
            IActiveOrdersRepository activeOrdersRepository, ITradingAccountService tradingAccountService)
        {
            _assetPairDictionaryService = assetPairDictionaryService;
            _matchingEngineEvents = matchingEngineEvents;
            _marketProfileRepository = marketProfileRepository;
            _activeOrdersRepository = activeOrdersRepository;
            _tradingAccountService = tradingAccountService;
        }

        public async Task UpdateMarketProfileAsync(IOrderBook order)
        {
            var assetPair = await _assetPairDictionaryService.GetByIdAsync(order.AssetPairId);
            if (assetPair == null)
            {
                //todo: should we log somewhere?
                return;
            }

            var existingProfile = await _marketProfileRepository.GetByIdAsync(order.AssetPairId);

            double bestPrice;

            bestPrice = order.IsBuy
                ? order.Prices.OrderBy(x => x.Price).Last().Price
                : order.Prices.OrderBy(x => x.Price).First().Price;

            AssetPairQuote newAssetPairQuote;

            if (existingProfile != null)
            {
                newAssetPairQuote = AssetPairQuote.Create(existingProfile);
                newAssetPairQuote.DateTime = order.Timestamp;

                if (!order.IsBuy)
                {
                    newAssetPairQuote.Bid = bestPrice;
                }
                else
                {
                    newAssetPairQuote.Ask = bestPrice;
                }

                await _marketProfileRepository.UpdateAsync(newAssetPairQuote);
            }
            else
            {
                newAssetPairQuote = new AssetPairQuote
                {
                    AssetPairId = order.AssetPairId,
                    DateTime = order.Timestamp
                };

                if (!order.IsBuy)
                {
                    newAssetPairQuote.Bid = bestPrice;
                }
                else
                {
                    newAssetPairQuote.Ask = bestPrice;
                }

                await _marketProfileRepository.AddAsync(newAssetPairQuote);
            }

            //todo: check pending orders if defined price is reached

            await _matchingEngineEvents.AssetPairPriceUpdatedAsync(newAssetPairQuote);

            await UpdateAcitveOrders(newAssetPairQuote);
        }

        private async Task UpdateAcitveOrders(AssetPairQuote assetPairQuote)
        {
            var orders = await _activeOrdersRepository.GetByAssetPairIdAsync(assetPairQuote.AssetPairId);

            var tradingAccounts = orders.Select(x => x.TradingAccountId).Distinct();

            foreach (var tradingAccount in tradingAccounts)
            {
                if (await _tradingAccountService.IsStopOutReachedAsync(tradingAccount))
                {
                    await _tradingAccountService.ForcedLiquidationAsync(tradingAccount);
                }
            }
        }
    }
}