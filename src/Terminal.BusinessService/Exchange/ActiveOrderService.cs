﻿using System;
using System.Threading.Tasks;
using AutoMapper;
using Lykke.Terminal.BusinessService.Infrastructure;
using Lykke.Terminal.Domain.Exchange;

namespace Lykke.Terminal.BusinessService.Exchange
{
    public class ActiveOrderService : IActiveOrderService, IApplicationService
    {
        private readonly IActiveOrdersRepository _activeOrdersRepository;
        private readonly IDoneOrderService _doneOrderService;
        private readonly IDoneOrdersRepository _doneOrdersRepository;
        private readonly IMarketProfileRepository _marketProfileRepository;

        public ActiveOrderService(IActiveOrdersRepository activeOrdersRepository,
            IDoneOrdersRepository doneOrdersRepository, IMarketProfileRepository marketProfileRepository,
            IDoneOrderService doneOrderService)
        {
            _activeOrdersRepository = activeOrdersRepository;
            _doneOrdersRepository = doneOrdersRepository;
            _marketProfileRepository = marketProfileRepository;
            _doneOrderService = doneOrderService;
        }

        public async Task MoveOrderToDoneAsync(IOrderBase order, OrderStatus status = OrderStatus.Done,
            OrderComment comment = OrderComment.None, double profitLoss = double.NaN)
        {
            if (!order.IsActiveOrder())
            {
                return;
            }

            var transactionHistory = await _doneOrderService.CreateDoneOrderAsync(order, status, comment, profitLoss);

            await _doneOrdersRepository.AddAsync(transactionHistory);

            await _activeOrdersRepository.DeteleAsync(order.Id);
        }

        public async Task<IOrderBase> CreateActiveOrderAsync(IOrderBase currentOrder, IAssetPairQuote currentQuote)
        {
            var order = OrderBase.Create(currentOrder);

            var assetPairQuote = currentQuote ?? await _marketProfileRepository.GetByIdAsync(currentOrder.AssetPairId);

            if (assetPairQuote == null)
            {
                throw new InvalidOperationException();
            }

            order.Price = currentOrder.OrderAction() == OrderAction.Buy
                ? assetPairQuote.Ask
                : assetPairQuote.Bid;

            order.Status = OrderStatus.Active;
            order.LastModified = DateTime.UtcNow;

            return order;
        }
    }
}