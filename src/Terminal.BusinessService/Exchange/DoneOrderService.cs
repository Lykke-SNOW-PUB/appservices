﻿using System;
using System.Threading.Tasks;
using AutoMapper;
using Lykke.Terminal.BusinessService.Infrastructure;
using Lykke.Terminal.Domain.Exchange;

namespace Lykke.Terminal.BusinessService.Exchange
{
    public class DoneOrderService : IDoneOrderService, IApplicationService
    {
        public Task<IDoneOrder> CreateDoneOrderAsync(IOrderBase order, OrderStatus orderStatus, OrderComment orderComment, double profitLoss = double.NaN)
        {
            var transactionHistory = Mapper.Map<DoneOrder>(order);
            transactionHistory.ProfitLoss = profitLoss;
            transactionHistory.LastModified = DateTime.UtcNow;
            transactionHistory.Status = orderStatus;
            transactionHistory.Comment = orderComment;

            return Task.FromResult((IDoneOrder) transactionHistory);
        }
    }
}