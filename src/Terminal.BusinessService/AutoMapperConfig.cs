﻿using System;
using System.Linq;
using AutoMapper;
using Lykke.Terminal.Domain.Accounts;
using Lykke.Terminal.Domain.ApplicationServices.Exchange;
using Lykke.Terminal.Domain.ApplicationServices.Trading;
using Lykke.Terminal.Domain.Exchange;
using Lykke.Terminal.Domain.Messaging.Dtos;

namespace Lykke.Terminal.BusinessService
{
    public class AutoMapperConfig : Profile
    {
        public AutoMapperConfig()
        {
            //todo: map from entity
            CreateMap<IAccountInfo, AccountInfo>();
            CreateMap<IDoneOrder, DoneOrder>();
            CreateMap<IOrderBase, OrderBase>();
            CreateMap<IAssetPairQuote, AssetPairQuote>();
            CreateMap<IOrderBase, OrderBase>();
            CreateMap<IOrderBase, DoneOrder>()
                .ForMember(dest => dest.ProfitLoss, opt => opt.Ignore())
                .ForMember(dest => dest.LastModified, opt => opt.MapFrom(x => DateTime.UtcNow));

            CreateMap<OrderBookMessageDto, OrderBook>()
                .ForMember(dest => dest.AssetPairId, opt => opt.MapFrom(x => x.AssetPair))
                .ForMember(dest => dest.Prices, opt => opt.MapFrom(s => s.Prices.Select(Mapper.Map<OrderBookDetailsDto>)))
                .ForMember(dest => dest.OrderBookId, opt => opt.Ignore());

            CreateMap<OrderBookDetailsDto, OrderBookLine>();

            CreateMap<OrderBook, OrderBookMessageDto>()
                .ForMember(dest => dest.AssetPair, opt => opt.MapFrom(x => x.AssetPairId))
                .ForMember(dest => dest.Prices, opt => opt.MapFrom(s => s.Prices.Select(Mapper.Map<OrderBookLine>)));
            CreateMap<OrderBookLine, OrderBookDetailsDto>();

            CreateMap<IAccountInfo, AccountInfoDto>().ForMember(dest => dest.TradingAccounts, opt => opt.Ignore());
            CreateMap<ITradingAccount, TradingAccountDto>();

            CreateMap<OpenOrderContext, OrderBase>()
                .ForMember(dest => dest.ClientId, opt => opt.MapFrom(x => x.AccountId))
                .ForMember(dest => dest.Id, opt => opt.Ignore())
                .ForMember(dest => dest.TradingAccountId, opt => opt.MapFrom(x => x.TransactioAccountId))
                .ForMember(dest => dest.CreatedAt, opt => opt.Ignore())
                .ForMember(dest => dest.Price, opt => opt.Ignore())
                .ForMember(dest => dest.Status, opt => opt.Ignore())
                .ForMember(dest => dest.LastModified, opt => opt.Ignore())
                .ForMember(dest => dest.Comment, opt => opt.Ignore())
                .ForMember(dest => dest.Commission, opt => opt.Ignore());
        }
    }
}