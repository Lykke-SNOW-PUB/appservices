﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Threading.Tasks;
using Lykke.Terminal.BusinessService.Infrastructure;
using Lykke.Terminal.Common.HttpRemoteRequests;
using Lykke.Terminal.Domain.Log;
using Lykke.Terminal.Domain.Settings;
using Lykke.Terminal.Domain.Trading;
using Microsoft.AspNetCore.WebUtilities;

namespace Lykke.Terminal.BusinessService.Trading
{
    public class TradeExecutionService : ITradeExecutionService, IApplicationService
    {
        private readonly IBaseSettings _baseSettings;
        private readonly HttpRequestClient _httpRequestClient;
        private readonly ILog _log;

        public TradeExecutionService(HttpRequestClient httpRequestClient, ILog log, IBaseSettings baseSettings)
        {
            _httpRequestClient = httpRequestClient;
            _log = log;
            _baseSettings = baseSettings;
        }

        public async Task<bool> CheckTradeRequestAsync(string orderId, string asset, double volume, double price)
        {
            var requestUri = new UriBuilder
            {
                Host = _baseSettings.TradeServerSettings.Host,
                Port = _baseSettings.TradeServerSettings.Port,
                Path = "request"
            };

            var side = volume > 0 ? "buy" : "sell";

            var queryStrings = new Dictionary<string, string>
            {
                {"id", orderId},
                {"asset", asset},
                {"volume", volume.ToString(CultureInfo.InvariantCulture)},
                {"price", price.ToString(CultureInfo.InvariantCulture)},
                {"side", side}
            };

            var requestUrl = QueryHelpers.AddQueryString(requestUri.ToString(), queryStrings);

            //todo: change format to json
            var requestResult = await _httpRequestClient.GetRequestAsync(requestUrl, "text/plain");

            if (string.IsNullOrEmpty(requestResult))
            {
                await
                    _log.WriteWarningAsync("TradeExecutionService", "CheckTradeRequest", null,
                        "Didn't receive response from force");

                return false;
            }

            if (requestResult.Contains("OK"))
            {
                return true;
            }

            return false;
        }
    }
}