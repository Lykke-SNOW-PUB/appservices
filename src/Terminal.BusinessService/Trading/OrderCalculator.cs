﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Lykke.Terminal.BusinessService.Infrastructure;
using Lykke.Terminal.Domain.Accounts;
using Lykke.Terminal.Domain.Assets;
using Lykke.Terminal.Domain.Dictionaries;
using Lykke.Terminal.Domain.Exchange;
using Lykke.Terminal.Domain.Trading;

namespace Lykke.Terminal.BusinessService.Trading
{
    public class OrderCalculator : IOrderCalculator, IApplicationService
    {
        private readonly IAssetPairDictionaryService _assetPairDictionaryService;
        private readonly IMarketProfileRepository _marketProfileRepository;

        public OrderCalculator(IMarketProfileRepository marketProfileRepository,
            IAssetPairDictionaryService assetPairDictionaryService)
        {
            _marketProfileRepository = marketProfileRepository;
            _assetPairDictionaryService = assetPairDictionaryService;
        }

        public virtual async Task<double> CalculateProfitLossAsync(double openPrice, double closePrice, double volume,
            IAssetPair assetPair, string baseAssetId)
        {
            var currencyRate = await GetCurrencyRateAsync(assetPair, baseAssetId);

            var result = (closePrice - openPrice)*volume/currencyRate;

            return result;
        }

        public async Task<double> CalculateMarginUsageAsync(IEnumerable<IOrderBase> orders,
            ITradingAccount tradingAccount)
        {
            var marginUsage = .0;

            foreach (var order in orders)
            {
                var assetPair = await _assetPairDictionaryService.GetByIdAsync(order.AssetPairId);

                var quote = await _marketProfileRepository.GetByIdAsync(order.AssetPairId);

                var price = order.OrderAction() == OrderAction.Buy ? quote.Ask : quote.Bid;

                var profitLoss = await CalculateProfitLossAsync(order.Price, price,
                    order.Volume, assetPair, tradingAccount.BaseAssetId);

                var margin = order.Volume*price/tradingAccount.Leverage;
                marginUsage += (tradingAccount.Balance + profitLoss)/margin;
            }

            return (1/marginUsage)*100;
        }

        private async Task<double> GetCurrencyRateAsync(IAssetPair assetPair, string baseAssetId)
        {
            if (assetPair.QuotingAssetId == baseAssetId)
            {
                return 1;
            }

            if ((assetPair.QuotingAssetId != baseAssetId) && (assetPair.BaseAssetId == baseAssetId))
            {
                var asset = await _marketProfileRepository.GetByIdAsync(assetPair.Id);

                return asset.Ask;
            }

            var requiredAssetPair =
                await _assetPairDictionaryService.GetByBaseAndQuotingIds(baseAssetId, assetPair.QuotingAssetId);

            if (requiredAssetPair != null)
            {
                var assetPairQuote = await _marketProfileRepository.GetByIdAsync(requiredAssetPair.Id);

                return assetPairQuote.Ask;
            }
            else
            {
                requiredAssetPair =
                    await _assetPairDictionaryService.GetByBaseAndQuotingIds(assetPair.QuotingAssetId, baseAssetId);

                var assetPairQuote = await _marketProfileRepository.GetByIdAsync(requiredAssetPair.Id);

                return 1/assetPairQuote.Ask;
            }
        }
    }
}