﻿using Lykke.Terminal.Common.Extenstions;
using Lykke.Terminal.Domain.Log;
using Microsoft.AspNetCore.Mvc.Filters;

namespace Lykke.Terminal.BusinessService.EventFilters
{
    public class UnhandledExceptionFilter : IExceptionFilter
    {
        private readonly ILog _errorLog;

        public UnhandledExceptionFilter(ILog errorLog)
        {
            _errorLog = errorLog;
        }

        public void OnException(ExceptionContext context)
        {
            _errorLog.WriteErrorAsync(context.Exception.Source, null, null, context.Exception).RunSync();
        }
    }
}