﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Lykke.Terminal.BusinessService.Infrastructure;
using Lykke.Terminal.Domain.Exchange;
using Lykke.Terminal.Domain.Messaging;
using Lykke.Terminal.Domain.Messaging.Dtos;
using Lykke.Terminal.Domain.Notifications;

namespace Lykke.Terminal.BusinessService.Notifications
{
    public class MatchingEngineEvents : IMatchingEngineEvents, IApplicationService
    {
        private readonly IBroker _broker;

        public MatchingEngineEvents(IBroker broker)
        {
            _broker = broker;
        }

        public async Task AccountUpdatedAsync(AccountInfoDto account)
        {
            await _broker.SendMessageAsync(MatchingEngineEventName.AccountUpdated, new List<string> {"Terminal"}, account);
        }

        public async Task AssetPairPriceUpdatedAsync(IAssetPairQuote assetPairQuote)
        {
            await
                _broker.SendMessageAsync(MatchingEngineEventName.AssetPairPriceUpdated, new List<string> {"Terminal"},
                    assetPairQuote);
        }

        public async Task ActiveOrdersUpdatedAsync(IEnumerable<OrderBase> orders)
        {
            await
                _broker.SendMessageAsync(MatchingEngineEventName.ActiveOrdersUpdated, new List<string> {"Terminal"},
                    orders);
        }

        public async Task OrderBookUpdatedAsync(OrderBookMessageDto message)
        {
            await
                _broker.SendMessageAsync(MatchingEngineEventName.OrderBookUpdated, new List<string> { "Terminal" },
                    message);
        }
    }
}