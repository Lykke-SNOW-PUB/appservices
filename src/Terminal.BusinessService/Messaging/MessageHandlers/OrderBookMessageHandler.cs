﻿using System;
using System.Collections.Concurrent;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Lykke.Terminal.Domain.Dictionaries;
using Lykke.Terminal.Domain.Exchange;
using Lykke.Terminal.Domain.Messaging;
using Lykke.Terminal.Domain.Messaging.Dtos;
using Lykke.Terminal.Domain.Messaging.Handlers;
using Lykke.Terminal.Domain.Notifications;

namespace Lykke.Terminal.BusinessService.Messaging.MessageHandlers
{
    public class OrderBookMessageHandler : IBrokerMessageHandler<OrderBookMessageDto>
    {
        private static readonly SemaphoreSlim _pool = new SemaphoreSlim(1, 1);
        private readonly IAssetPairDictionaryService _assetPairDictionaryService;
        private readonly IMarketProfileService _marketProfileService;
        private readonly IMatchingEngineEvents _matchingEngineEvents;
        private readonly IOrderBookRepository _orderBookRepository;

        private ConcurrentQueue<OrderBookMessageDto> _queue;

        public OrderBookMessageHandler(IOrderBookRepository orderBookRepository,
            IMatchingEngineEvents matchingEngineEvents, IMarketProfileService marketProfileService,
            IAssetPairDictionaryService assetPairDictionaryService)
        {
            _orderBookRepository = orderBookRepository;
            _matchingEngineEvents = matchingEngineEvents;
            _marketProfileService = marketProfileService;
            _assetPairDictionaryService = assetPairDictionaryService;
            _queue = new ConcurrentQueue<OrderBookMessageDto>();
        }

        public async Task HandleAsync(OrderBookMessageDto message, IBrokerMessageContext context)
        {
            _queue.Enqueue(message);

            await _pool.WaitAsync();

            OrderBookMessageDto msg;

            if (_queue.TryDequeue(out msg))
            {
                await HandleMessageAsync(msg);
            }

            _pool.Release();
        }

        private async Task HandleMessageAsync(OrderBookMessageDto message)
        {
            var assetPair = await _assetPairDictionaryService.GetByIdAsync(message.AssetPair);
            if (assetPair == null)
            {
                //todo: should we log somewhere?
                return;
            }

            var order = Mapper.Map<OrderBook>(message);

            var existingOrder = await _orderBookRepository.GetByAssetPairIdAndBuy(order);

            if ((existingOrder != null) && existingOrder.Any())
            {
                //todo: refactor this
                order.OrderBookId = existingOrder.First().OrderBookId;
                await _orderBookRepository.UpdateAsync(order);
            }
            else
            {
                await _orderBookRepository.AddAsync(order);
            }

            await _matchingEngineEvents.OrderBookUpdatedAsync(message);

            await _marketProfileService.UpdateMarketProfileAsync(order);
        }
    }
}