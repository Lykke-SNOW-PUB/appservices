﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Lykke.Terminal.BusinessService.Infrastructure;
using Lykke.Terminal.Domain.ApplicationServices.Exchange;
using Lykke.Terminal.Domain.Exchange;

namespace Lykke.Terminal.BusinessService.ApplicationServices.Exchange
{
    public class ActiveOrderAppService : IActiveOrderAppService, IApplicationService
    {
        private readonly IActiveOrdersRepository _marketOrdersRepository;

        public ActiveOrderAppService(IActiveOrdersRepository marketOrdersRepository)
        {
            _marketOrdersRepository = marketOrdersRepository;
        }

        public async Task<IEnumerable<OrderBase>> GetActiveOrdersAsync(string clientId)
        {
            var activeOrders = await _marketOrdersRepository.GetByClientId(clientId);

            return activeOrders.Select(Mapper.Map<OrderBase>);
        }
    }
}