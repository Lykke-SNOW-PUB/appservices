﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Lykke.Terminal.BusinessService.Infrastructure;
using Lykke.Terminal.Domain.ApplicationServices.Exchange;
using Lykke.Terminal.Domain.Exchange;

namespace Lykke.Terminal.BusinessService.ApplicationServices.Exchange
{
    public class MarketProfileAppService : IMarketProfileAppService, IApplicationService
    {
        private readonly IMarketProfileRepository _marketProfileRepository;

        public MarketProfileAppService(IMarketProfileRepository marketProfileRepository)
        {
            _marketProfileRepository = marketProfileRepository;
        }

        public async Task<IEnumerable<AssetPairQuote>> GetMarketProfileAsync()
        {
            var marketProfile = await _marketProfileRepository.GetAllAsync();

            return marketProfile.Select(Mapper.Map<AssetPairQuote>);
        }

    }
}