﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Lykke.Terminal.BusinessService.Infrastructure;
using Lykke.Terminal.Domain.ApplicationServices.Exchange;
using Lykke.Terminal.Domain.Exchange;

namespace Lykke.Terminal.BusinessService.ApplicationServices.Exchange
{
    public class TransactionHistoryAppService : ITransactionHistoryAppService, IApplicationService
    {
        private readonly IDoneOrdersRepository _transactionHistoryRepository;

        public TransactionHistoryAppService(IDoneOrdersRepository transactionHistoryRepository)
        {
            _transactionHistoryRepository = transactionHistoryRepository;
        }

        public async Task<IEnumerable<DoneOrder>> GetTransactionHistoryAsync(string accountId)
        {
            var history = await _transactionHistoryRepository.GetByClientId(accountId);

            return history.Select(Mapper.Map<DoneOrder>);
        }
    }
}