﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Lykke.Terminal.BusinessService.Infrastructure;
using Lykke.Terminal.Domain.ApplicationServices.Exchange;
using Lykke.Terminal.Domain.Exchange;

namespace Lykke.Terminal.BusinessService.ApplicationServices.Exchange
{
    public class OrderBookAppService : IOrderBookAppService, IApplicationService
    {
        private readonly IOrderBookRepository _orderBookRepository;

        public OrderBookAppService(IOrderBookRepository orderBookRepository)
        {
            _orderBookRepository = orderBookRepository;
        }

        public async Task<IEnumerable<OrderBookDto>> GetOrderBookAsync()
        {
            var orderBooks = await _orderBookRepository.GetAllAsync();

            var result = new List<OrderBookDto>();

            foreach (var order in orderBooks.GroupBy(x => x.AssetPairId))
            {
                var orderBook = new OrderBookDto
                {
                    Symbol = order.Key,
                    BuyOrders =
                        order.Where(o => o.IsBuy).SelectMany(o => o.Prices.Select(Mapper.Map<OrderBookDetailsDto>)),
                    SellOrders =
                        order.Where(o => !o.IsBuy)
                            .SelectMany(o => o.Prices.Select(Mapper.Map<OrderBookDetailsDto>)).OrderByDescending(o => o.Price)
                };

                result.Add(orderBook);
            }

            return result;
        }
    }
}