﻿using System;
using System.Threading.Tasks;
using AutoMapper;
using Lykke.Terminal.BusinessService.Infrastructure;
using Lykke.Terminal.Domain.Accounts;
using Lykke.Terminal.Domain.ApplicationServices.Accounts;
using Lykke.Terminal.Domain.ApplicationServices.Trading;
using Lykke.Terminal.Domain.Dictionaries;
using Lykke.Terminal.Domain.Exchange;
using Lykke.Terminal.Domain.Notifications;
using Lykke.Terminal.Domain.Trading;

namespace Lykke.Terminal.BusinessService.ApplicationServices.Trading
{
    public class TradingAppService : ITradingAppService, IApplicationService
    {
        private readonly IAccountInfoRepository _accountInfoRepository;
        private readonly IAccountAppService _accountService;
        private readonly IActiveOrderService _activeOrderService;
        private readonly IActiveOrdersRepository _activeOrdersRepository;
        private readonly IAssetPairDictionaryService _assetPairDictionaryService;
        private readonly IDoneOrderService _doneOrderService;
        private readonly IDoneOrdersRepository _doneOrdersRepository;
        private readonly IMarketProfileRepository _marketProfileRepository;
        private readonly IMatchingEngineEvents _matchingEngineEvents;
        private readonly IOrderCalculator _orderCalculator;
        private readonly IPendingOrdersRepository _pendingOrdersRepository;
        private readonly ITradeExecutionService _tradeExecutionService;
        private readonly ITradingAccountRepository _tradingAccountRepository;
        private readonly ITradingAccountService _tradingAccountService;

        public TradingAppService(IAccountInfoRepository accountInfoRepository,
            IActiveOrdersRepository activeOrdersRepository, IMarketProfileRepository marketProfileRepository,
            IOrderCalculator orderCalculator, IMatchingEngineEvents matchingEngineEvents,
            IPendingOrdersRepository pendingOrdersRepository,
            IAssetPairDictionaryService assetPairDictionaryService, ITradingAccountRepository tradingAccountRepository,
            IAccountAppService accountService, ITradingAccountService tradingAccountService,
            IActiveOrderService activeOrderService, IDoneOrdersRepository doneOrdersRepository,
            ITradeExecutionService tradeExecutionService, IDoneOrderService doneOrderService)
        {
            _accountInfoRepository = accountInfoRepository;
            _activeOrdersRepository = activeOrdersRepository;
            _marketProfileRepository = marketProfileRepository;
            _orderCalculator = orderCalculator;
            _matchingEngineEvents = matchingEngineEvents;
            _pendingOrdersRepository = pendingOrdersRepository;
            _assetPairDictionaryService = assetPairDictionaryService;
            _tradingAccountRepository = tradingAccountRepository;
            _accountService = accountService;
            _tradingAccountService = tradingAccountService;
            _activeOrderService = activeOrderService;
            _doneOrdersRepository = doneOrdersRepository;
            _tradeExecutionService = tradeExecutionService;
            _doneOrderService = doneOrderService;
        }

        public async Task OpenOrderAsync(OpenOrderContext context)
        {
            var orderInfo = Mapper.Map<OrderBase>(context);

            orderInfo.Id = Guid.NewGuid().ToString();
            orderInfo.CreatedAt = DateTime.UtcNow;
            orderInfo.Price = double.NaN;

            if (!double.IsNaN(context.DefinedPrice) && (Math.Abs(context.DefinedPrice) > 0))
            {
                orderInfo.Status = OrderStatus.WaitingForExecution;

                await _pendingOrdersRepository.AddAsync(orderInfo);
            }
            else
            {
                try
                {
                    var order = await _activeOrderService.CreateActiveOrderAsync(orderInfo, null);

                    var sucessfullyExecuted = await _tradeExecutionService.CheckTradeRequestAsync(order.Id,
                        order.AssetPairId, order.Volume, order.Price);

                    if (sucessfullyExecuted)
                    {
                        await _activeOrdersRepository.AddAsync(order);
                    }
                    else
                    {
                        var transactionHistory =
                            await
                                _doneOrderService.CreateDoneOrderAsync(order, OrderStatus.Rejected,
                                    OrderComment.TradeService);

                        await _doneOrdersRepository.AddAsync(transactionHistory);
                    }

                    //todo: check if stop out is reached
                }
                catch (InvalidOperationException)
                {
                    var doneOrder = await
                        _doneOrderService.CreateDoneOrderAsync(orderInfo, OrderStatus.Cancelled,
                            OrderComment.NoQuote);

                    await _doneOrdersRepository.AddAsync(doneOrder);

                    //todo: return a special status to the front-end
                }
            }
        }

        public async Task CloseOrderAsync(string accountId, string orderId)
        {
            var account = await _accountInfoRepository.GetByIdAsync(accountId);

            if (account == null)
            {
                throw new InvalidOperationException();
            }

            var activeOrder = await _activeOrdersRepository.GetByIdAsync(orderId);

            var assetPair = await _assetPairDictionaryService.GetByIdAsync(activeOrder.AssetPairId);

            var quote = await _marketProfileRepository.GetByIdAsync(activeOrder.AssetPairId);

            var tradingAccount = await _tradingAccountRepository.GetByIdAsync(activeOrder.TradingAccountId);

            var price = activeOrder.OrderAction() == OrderAction.Buy ? quote.Ask : quote.Bid;

            var profitLoss =
                await _orderCalculator.CalculateProfitLossAsync(activeOrder.Price, price,
                    activeOrder.Volume, assetPair, tradingAccount.BaseAssetId);

            var doneOrder = Mapper.Map<OrderBase>(activeOrder);
            doneOrder.Price = price;
            await _activeOrderService.MoveOrderToDoneAsync(doneOrder, profitLoss: profitLoss);

            var updatedAccount = TradingAccount.Create(tradingAccount);
            updatedAccount.Balance += profitLoss;
            await _tradingAccountRepository.UpdateAsync(updatedAccount);

            var dto = await _accountService.GetAccountInfoAsync(accountId);

            await _matchingEngineEvents.AccountUpdatedAsync(dto);
        }
    }
}