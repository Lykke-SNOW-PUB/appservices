﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Lykke.Terminal.BusinessService.Infrastructure;
using Lykke.Terminal.Domain.Accounts;
using Lykke.Terminal.Domain.ApplicationServices.Accounts;
using Lykke.Terminal.Domain.Messaging.Dtos;

namespace Lykke.Terminal.BusinessService.ApplicationServices.Account
{
    public class AccountAppService : IAccountAppService, IApplicationService
    {
        private readonly IAccountInfoRepository _accountInfoRepository;
        private readonly ITradingAccountRepository _tradingAccountRepository;

        public AccountAppService(IAccountInfoRepository accountInfoRepository,
            ITradingAccountRepository tradingAccountRepository)
        {
            _accountInfoRepository = accountInfoRepository;
            _tradingAccountRepository = tradingAccountRepository;
        }

        public async Task UpdateAccountInfoAsync(AccountInfo context)
        {
            var account = await _accountInfoRepository.GetByIdAsync(context.AccountId);

            if (account == null)
            {
                throw new InvalidOperationException();
            }

            await _accountInfoRepository.UpdateAsync(context);
        }

        public async Task<AccountInfoDto> GetAccountInfoAsync(string accountId)
        {
            //todo: remove default account creation
            var account = await _accountInfoRepository.GetByIdAsync(accountId);

            if (account == null)
            {
                account = AccountInfo.CreateDefault(accountId);
                await _accountInfoRepository.AddAsync(account);
            }

            var tradeAccounts = await _tradingAccountRepository.GetByClientId(accountId);

            if ((tradeAccounts == null) || !tradeAccounts.Any())
            {
                tradeAccounts = new List<ITradingAccount> {TradingAccount.CreateDefault(accountId)};
                await _tradingAccountRepository.AddAsync(tradeAccounts.FirstOrDefault());
            }

            var accountInfo = Mapper.Map<AccountInfoDto>(account);
            accountInfo.TradingAccounts = tradeAccounts.Select(Mapper.Map<TradingAccountDto>);

            return accountInfo;
        }
    }
}