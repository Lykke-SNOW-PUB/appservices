﻿using System.Threading.Tasks;
using Lykke.Terminal.BusinessService.Infrastructure;
using Lykke.Terminal.Domain.Accounts;
using Lykke.Terminal.Domain.Exchange;
using Lykke.Terminal.Domain.Trading;

namespace Lykke.Terminal.BusinessService.Accounts
{
    public class TradingAccountService : ITradingAccountService, IApplicationService
    {
        private readonly IActiveOrderService _activeOrderService;
        private readonly IActiveOrdersRepository _activeOrdersRepository;
        private readonly IOrderCalculator _orderCalculator;
        private readonly ITradingAccountRepository _tradingAccountRepository;

        public TradingAccountService(IOrderCalculator orderCalculator,
            ITradingAccountRepository tradingAccountRepository, IActiveOrdersRepository activeOrdersRepository,
            IActiveOrderService activeOrderService)
        {
            _orderCalculator = orderCalculator;
            _tradingAccountRepository = tradingAccountRepository;
            _activeOrdersRepository = activeOrdersRepository;
            _activeOrderService = activeOrderService;
        }

        public async Task<bool> IsStopOutReachedAsync(string tradingAccountId)
        {
            var tradingAccount = await _tradingAccountRepository.GetByIdAsync(tradingAccountId);

            var orders = await _activeOrdersRepository.GetByTradingAccountIdAsync(tradingAccountId);

            var marginUsage = await _orderCalculator.CalculateMarginUsageAsync(orders, tradingAccount);

            return tradingAccount.StopOut.CompareTo(100-marginUsage) >= 0;
        }

        public async Task ForcedLiquidationAsync(string tradingAccountId)
        {
            var orders = await _activeOrdersRepository.GetByTradingAccountIdAsync(tradingAccountId);

            foreach (var order in orders)
            {
                await _activeOrderService.MoveOrderToDoneAsync(order, OrderStatus.Cancelled, OrderComment.StopOut);
            }
        }
    }
}