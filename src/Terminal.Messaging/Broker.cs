﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Lykke.Terminal.Common.Utils;
using Lykke.Terminal.Domain.Log;
using Lykke.Terminal.Domain.Messaging;
using RawRabbit.Configuration.Exchange;
using RawRabbit.vNext.Disposable;

namespace Lykke.Terminal.Messaging
{
    public class Broker : IBroker
    {
        private static readonly ILog Log = DependencyContainer.Instance.GetInstance<ILog>();
        private readonly IBusClient _busClient;

        public Broker(IBusClient busClient)
        {
            _busClient = busClient;
        }

        public async Task SendMessageAsync<T>(string topic, IEnumerable<string> subscriberList, T message)
        {
            try
            {
                var subscribers = string.Join(".", subscriberList);
                await
                    _busClient.PublishAsync(message,
                        configuration:
                        cfg =>
                            cfg.WithExchange(e => e.WithName(topic).WithType(ExchangeType.Topic))
                                .WithRoutingKey(subscribers));
            }
            catch (Exception ex)
            {
                await
                    Log.WriteWarningAsync("TerminalApplication", "Broker", null,
                        $"Unable to send message, exception: {ex}");
                // throw;
            }
        }

        public async Task SubscribeAsync<T>(string topic, IEnumerable<string> subscriberList,
            Func<T, IBrokerMessageContext, Task> messageCallbackAsyncFunc,
            BrokerExchangeType type = BrokerExchangeType.Topic)
        {
            try
            {
                var exchangeType = (ExchangeType) Enum.Parse(typeof(ExchangeType), type.ToString());

                var subscribers = subscriberList == null ? null : string.Join(".", subscriberList);

                _busClient.SubscribeAsync<T>(
                    async (message, context) => await messageCallbackAsyncFunc(message, context as BrokerMessageContext),
                    cfg =>
                        cfg.WithExchange(e => e.WithName(topic).WithType(exchangeType))
                            .WithRoutingKey(subscribers));
            }
            catch (Exception ex)
            {
                await Log.WriteWarningAsync("TerminalApplication", "Broker", null,
                    $"Unable to create subscription, exception: {ex}");
                // throw;
            }
        }
    }
}