﻿using Lykke.Terminal.Domain.Messaging;
using StructureMap;

namespace Lykke.Terminal.Messaging
{
    public class MessagingConfig : Registry
    {
        public MessagingConfig(IBrokerConfiguration settingsBrokerConfiguration)
        {
            var connectionFactory = BrokerConnectionFactory.Create(settingsBrokerConfiguration);
            var broker = connectionFactory.GetBroker();

            For<IBroker>().Add(broker);
        }
    }
}