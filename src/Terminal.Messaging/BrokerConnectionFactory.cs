﻿using Lykke.Terminal.Domain.Messaging;

namespace Lykke.Terminal.Messaging
{
    public class BrokerConnectionFactory
    {
        public static IBrokerConnection Create(IBrokerConfiguration config)
        {
            return new BrokerConnection(config as BrokerConfiguration);
        }
    }
}