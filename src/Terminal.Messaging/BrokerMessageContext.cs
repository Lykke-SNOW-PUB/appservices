﻿using Lykke.Terminal.Domain.Messaging;
using RawRabbit.Context;

namespace Lykke.Terminal.Messaging
{
    public class BrokerMessageContext : MessageContext, IBrokerMessageContext
    {
    }
}