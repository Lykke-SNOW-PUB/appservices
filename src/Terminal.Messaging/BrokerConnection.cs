﻿using System;
using Lykke.Terminal.Domain.Messaging;
using Polly;
using RabbitMQ.Client.Exceptions;
using RawRabbit.vNext;
using RawRabbit.vNext.Disposable;

namespace Lykke.Terminal.Messaging
{
    public class BrokerConnection : IBrokerConnection
    {
        private readonly IBroker _broker;
        private readonly IBusClient _busClient;

        public BrokerConnection(BrokerConfiguration optionsAccessor)
        {
            var policy = Policy.Handle<ConnectFailureException>()
                .WaitAndRetry(10, retryAttempt => TimeSpan.FromSeconds(Math.Pow(2, retryAttempt)));
            _busClient = policy.Execute(() => BusClientFactory.CreateDefault(optionsAccessor));

            _broker = new Broker(_busClient);
        }

        public IBroker GetBroker()
        {
            return _broker;
        }

        public void Dispose()
        {
            _busClient.Dispose();
        }
    }
}