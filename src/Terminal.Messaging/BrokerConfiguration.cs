﻿using System.Collections.Generic;
using Lykke.Terminal.Domain.Messaging;
using Microsoft.CSharp.RuntimeBinder;
using RawRabbit.Configuration;
using RawRabbit.Extensions.Client;

namespace Lykke.Terminal.Messaging
{
    public class BrokerConfiguration : RawRabbitConfiguration, IBrokerConfiguration
    {
        public static BrokerConfiguration Default => new BrokerConfiguration
        {
            Username = Local.Username,
            Password = Local.Password,
            Port = Local.Port,
            Hostnames = new List<string>(Local.Hostnames) { "rabbitmq" },
            VirtualHost = Local.VirtualHost
        };
    }
}