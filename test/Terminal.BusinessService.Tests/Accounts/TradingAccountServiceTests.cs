﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Lykke.Terminal.BusinessService.Accounts;
using Lykke.Terminal.Domain.Accounts;
using Lykke.Terminal.Domain.Exchange;
using Lykke.Terminal.Domain.Trading;
using NSubstitute;
using Xunit;

namespace Lykke.Terminal.BusinessService.Tests.Accounts
{
    public class TradingAccountServiceTests
    {
        [Fact]
        public async Task StopOut_IsReached_IfAccountStopOut_IsEqualToMarginUsage()
        {
            //arrange
            var marginUsageValue = 80;
            var tradingAccount = new TradingAccount
            {
                StopOut = 20
            };

            var tradingAccountService = CreateTradingAccountServiceForStopOut(tradingAccount, marginUsageValue);
            var result = await tradingAccountService.IsStopOutReachedAsync("22");

            Assert.Equal(true, result);
        }

        [Fact]
        public async Task StopOut_IsNotReached_IfAccountStopOut_IsLessThanMarginUsage()
        {
            //arrange
            var marginUsageValue = 80;
            var tradingAccount = new TradingAccount
            {
                StopOut = 10
            };

            var tradingAccountService = CreateTradingAccountServiceForStopOut(tradingAccount, marginUsageValue);
            var result = await tradingAccountService.IsStopOutReachedAsync("22");

            Assert.Equal(false, result);
        }

        [Fact]
        public async Task StopOut_IsReached_IfAccountStopOut_IsGreaterThanMarginUsage()
        {
            //arrange
            var marginUsageValue = 80;
            var tradingAccount = new TradingAccount
            {
                StopOut = 30
            };

            var tradingAccountService = CreateTradingAccountServiceForStopOut(tradingAccount, marginUsageValue);
            var result = await tradingAccountService.IsStopOutReachedAsync("22");

            Assert.Equal(true, result);
        }

        private static TradingAccountService CreateTradingAccountServiceForStopOut(ITradingAccount tradingAccount,
            int marginUsageValue)
        {
            var orderCalculatorFake = Substitute.For<IOrderCalculator>();
            var tradingAccountRepositoryFake = Substitute.For<ITradingAccountRepository>();
            var activeOrdersRepositoryFake = Substitute.For<IActiveOrdersRepository>();
            var activeOrdersServiceFake = Substitute.For<IActiveOrderService>();

            tradingAccountRepositoryFake.GetByIdAsync(Arg.Any<string>()).ReturnsForAnyArgs(tradingAccount);
            activeOrdersRepositoryFake.GetByTradingAccountIdAsync(Arg.Any<string>())
                .ReturnsForAnyArgs((IEnumerable<IOrderBase>) null);
            orderCalculatorFake.CalculateMarginUsageAsync(Arg.Any<IEnumerable<IOrderBase>>(), Arg.Any<ITradingAccount>())
                .ReturnsForAnyArgs(marginUsageValue);

            //act
            var tradingAccountService = new TradingAccountService(orderCalculatorFake, tradingAccountRepositoryFake,
                activeOrdersRepositoryFake, activeOrdersServiceFake);
            return tradingAccountService;
        }
    }
}