﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Lykke.Terminal.BusinessService.Trading;
using Lykke.Terminal.Domain.Accounts;
using Lykke.Terminal.Domain.Assets;
using Lykke.Terminal.Domain.Dictionaries;
using Lykke.Terminal.Domain.Exchange;
using NSubstitute;
using Xunit;

namespace Lykke.Terminal.BusinessService.Tests.Trading
{
    public class OrderCalculatorTests
    {
        private OrderCalculator MockOrderCalculator()
        {
            var assetPairQuoteRepository = Substitute.For<IMarketProfileRepository>();
            var gbpUsd = new AssetPairQuote
            {
                AssetPairId = "GBPUSD",
                Ask = 1.5555
            };
            var usdChf = new AssetPairQuote
            {
                AssetPairId = "USDCHF",
                Ask = 1.2222
            };
            var usdJpy = new AssetPairQuote
            {
                AssetPairId = "USDJPY",
                Ask = 1.3333
            };
            assetPairQuoteRepository.GetByIdAsync(gbpUsd.AssetPairId).Returns(gbpUsd);
            assetPairQuoteRepository.GetByIdAsync(usdChf.AssetPairId).Returns(usdChf);
            assetPairQuoteRepository.GetByIdAsync(usdJpy.AssetPairId).Returns(usdJpy);

            //for margin usage
            var lkkUsd = new AssetPairQuote
            {
                AssetPairId = "LKKUSD",
                Ask = 75.00
            };
            assetPairQuoteRepository.GetByIdAsync(lkkUsd.AssetPairId).Returns(lkkUsd);

            var assetPairDictionaryServiceFake = Substitute.For<IAssetPairDictionaryService>();
            assetPairDictionaryServiceFake.GetByBaseAndQuotingIds("USD", "JPY").Returns(new AssetPair {Id = "USDJPY"});
            assetPairDictionaryServiceFake.GetByBaseAndQuotingIds("GBP", "USD").Returns(new AssetPair {Id = "GBPUSD"});
            assetPairDictionaryServiceFake.GetByBaseAndQuotingIds("USD", "GBP").Returns((IAssetPair) null);
            assetPairDictionaryServiceFake.GetByIdAsync("GBPUSD").Returns(new AssetPair
            {
                Id = "GBPUSD",
                BaseAssetId = "GBP",
                QuotingAssetId = "USD"
            });
            assetPairDictionaryServiceFake.GetByIdAsync("LKKUSD").Returns(new AssetPair
            {
                Id = "LKKUSD",
                BaseAssetId = "LKK",
                QuotingAssetId = "USD"
            });

            var calculator = Substitute.ForPartsOf<OrderCalculator>(assetPairQuoteRepository, assetPairDictionaryServiceFake);
            return calculator;
        }

        [Fact]
        public async Task BasePair_IsBaseAsset_In_AssetPair_CorrectCalculation()
        {
            //data
            var openPrice = 1.4351;
            var closePrice = 1.4361;
            var volume = 1000;
            var baseAssetId = "USD";
            var assetPair = new AssetPair
            {
                Id = "USDCHF",
                BaseAssetId = "USD",
                QuotingAssetId = "CHF"
            };

            //arrange
            var orderCalculator = MockOrderCalculator();
            var profitLoss =
                await orderCalculator.CalculateProfitLossAsync(openPrice, closePrice, volume, assetPair, baseAssetId);

            Assert.Equal(0.8182, Math.Round(profitLoss, 4));
        }

        [Fact]
        public async Task BasePair_IsNot_In_AssetPair_AsserPairDoesntExist_CorrectCalculation()
        {
            //data
            var openPrice = 1.4351;
            var closePrice = 1.4361;
            var volume = 1000;
            var baseAssetId = "USD";
            var assetPair = new AssetPair
            {
                Id = "EURGBP",
                BaseAssetId = "EUR",
                QuotingAssetId = "GBP"
            };

            //arrange
            var orderCalculator = MockOrderCalculator();
            var profitLoss =
                await orderCalculator.CalculateProfitLossAsync(openPrice, closePrice, volume, assetPair, baseAssetId);

            Assert.Equal(1.5555, Math.Round(profitLoss, 4));
        }

        [Fact]
        public async Task BasePair_IsNot_In_AssetPair_AsserPairExists_CorrectCalculation()
        {
            //data
            var openPrice = 1.4351;
            var closePrice = 1.4361;
            var volume = 1000;
            var baseAssetId = "USD";
            var assetPair = new AssetPair
            {
                Id = "EURJPY",
                BaseAssetId = "EUR",
                QuotingAssetId = "JPY"
            };

            //arrange
            var orderCalculator = MockOrderCalculator();
            var profitLoss =
                await orderCalculator.CalculateProfitLossAsync(openPrice, closePrice, volume, assetPair, baseAssetId);

            Assert.Equal(0.75, Math.Round(profitLoss, 4));
        }

        [Fact]
        public async Task BasePair_IsQuotingAsset_In_AssetPair_CorrectCalculation()
        {
            //data
            var openPrice = 1.4351;
            var closePrice = 1.4361;
            var volume = 1000;
            var baseAssetId = "USD";
            var assetPair = new AssetPair
            {
                Id = "GBPUSD",
                BaseAssetId = "GBP",
                QuotingAssetId = "USD"
            };

            //arrange
            var orderCalculator = MockOrderCalculator();
            var profitLoss =
                await orderCalculator.CalculateProfitLossAsync(openPrice, closePrice, volume, assetPair, baseAssetId);

            Assert.Equal(1.0, Math.Round(profitLoss, 4));
        }

        [Fact]
        public async Task MarginUsage_CorrectCalculation()
        {
            //data
            var orders = new List<OrderBase>
            {
                new OrderBase
                {
                    AssetPairId = "LKKUSD",
                    Volume = 100,
                    Price = 75.00
                }
            };
            var tradingAccount = new TradingAccount
            {
                Balance = 1000,
                Leverage = 10,
                BaseAssetId = "USD"
            };

            //arrange
            var orderCalculator = MockOrderCalculator();
            orderCalculator.CalculateProfitLossAsync(Arg.Any<double>(), Arg.Any<double>(), Arg.Any<double>(), Arg.Any<IAssetPair>(),
                Arg.Any<string>()).ReturnsForAnyArgs(-100);

            var marginUsage = await orderCalculator.CalculateMarginUsageAsync(orders, tradingAccount);

            Assert.Equal(83.3, Math.Round(marginUsage, 1));
        }
    }
}