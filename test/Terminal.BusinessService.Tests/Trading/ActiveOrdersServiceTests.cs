﻿using System.Threading.Tasks;
using AutoMapper;
using Lykke.Terminal.BusinessService.Exchange;
using Lykke.Terminal.Domain.Exchange;
using NSubstitute;
using Xunit;

namespace Lykke.Terminal.BusinessService.Tests.Trading
{
    public class ActiveOrdersServiceTests
    {
        [Fact]
        public async Task MoverToDoneOrders_IsRemoved_FromActive()
        {
            //arrange
            var activeOrdersRepositoryFake = Substitute.For<IActiveOrdersRepository>();
            var doneOrdersRepositoryFake = Substitute.For<IDoneOrdersRepository>();
            var marketProfileRepositoryFake = Substitute.For<IMarketProfileRepository>();
            var doneOrderServiceFake = Substitute.For<IDoneOrderService>();

            activeOrdersRepositoryFake.DeteleAsync(Arg.Any<string>()).ReturnsForAnyArgs(Task.FromResult);
            doneOrdersRepositoryFake.AddAsync(Arg.Any<IDoneOrder>()).ReturnsForAnyArgs(Task.FromResult);
            Mapper.Initialize(cfg => cfg.AddProfile(new AutoMapperConfig()));

            //act
            var orderBaseService = new ActiveOrderService(activeOrdersRepositoryFake, doneOrdersRepositoryFake,
                marketProfileRepositoryFake, doneOrderServiceFake);
            await orderBaseService.MoveOrderToDoneAsync(new DoneOrder());

            //assert
            await activeOrdersRepositoryFake.Received().DeteleAsync(Arg.Any<string>());
            await doneOrdersRepositoryFake.Received().AddAsync(Arg.Any<IDoneOrder>());
        }

        [Fact]
        public async Task MoverToDoneOrders_WorksForActiveOrders_Only()
        {
            //arrange
            var activeOrdersRepositoryFake = Substitute.For<IActiveOrdersRepository>();
            var doneOrdersRepositoryFake = Substitute.For<IDoneOrdersRepository>();
            var marketProfileRepositoryFake = Substitute.For<IMarketProfileRepository>();
            var doneOrderServiceFake = Substitute.For<IDoneOrderService>();

            activeOrdersRepositoryFake.DeteleAsync(Arg.Any<string>()).ReturnsForAnyArgs(Task.FromResult);
            doneOrdersRepositoryFake.AddAsync(Arg.Any<IDoneOrder>()).ReturnsForAnyArgs(Task.FromResult);
            Mapper.Initialize(cfg => cfg.AddProfile(new AutoMapperConfig()));

            //act
            var orderBaseService = new ActiveOrderService(activeOrdersRepositoryFake, doneOrdersRepositoryFake,
                marketProfileRepositoryFake, doneOrderServiceFake);
            await orderBaseService.MoveOrderToDoneAsync(new DoneOrder {DefinedPrice = 12});

            //assert
            await activeOrdersRepositoryFake.DidNotReceive().DeteleAsync(Arg.Any<string>());
            await doneOrdersRepositoryFake.DidNotReceive().AddAsync(Arg.Any<IDoneOrder>());
        }
    }
}