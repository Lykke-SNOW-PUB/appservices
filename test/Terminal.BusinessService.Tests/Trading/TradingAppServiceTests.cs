﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Lykke.Terminal.BusinessService.ApplicationServices.Trading;
using Lykke.Terminal.Domain.Accounts;
using Lykke.Terminal.Domain.ApplicationServices.Accounts;
using Lykke.Terminal.Domain.ApplicationServices.Trading;
using Lykke.Terminal.Domain.Assets;
using Lykke.Terminal.Domain.Dictionaries;
using Lykke.Terminal.Domain.Exchange;
using Lykke.Terminal.Domain.Messaging.Dtos;
using Lykke.Terminal.Domain.Notifications;
using Lykke.Terminal.Domain.Trading;
using NSubstitute;
using Xunit;

namespace Lykke.Terminal.BusinessService.Tests.Trading
{
    public class TradingAppServiceTests
    {
        private TradingAppService CreateTradingService(IActiveOrdersRepository ordersRepositoryFake,
            IPendingOrdersRepository limitOrdersRepositoryFake, ITradingAccountRepository tradingAccountRepository,
            IActiveOrderService orderBaseService, IMatchingEngineEvents matchingEngineEvents)
        {
            var accountInfoRepositoryFake = Substitute.For<IAccountInfoRepository>();
            var accountServiceFake = Substitute.For<IAccountAppService>();
            var activeLimitOrdersRepositoryFake = limitOrdersRepositoryFake ??
                                                  Substitute.For<IPendingOrdersRepository>();
            var assetPairDictionaryServiceFake = Substitute.For<IAssetPairDictionaryService>();
            var marketOrdersRepositoryFake = ordersRepositoryFake ?? Substitute.For<IActiveOrdersRepository>();
            var marketProfileRepositoryFake = Substitute.For<IMarketProfileRepository>();
            var matchingEngineEventsFake = matchingEngineEvents ?? Substitute.For<IMatchingEngineEvents>();
            var orderCalculatorFake = Substitute.For<IOrderCalculator>();
            var tradingAccountRepositoryFake = tradingAccountRepository ?? Substitute.For<ITradingAccountRepository>();
            var tradingAccountService = Substitute.For<ITradingAccountService>();
            var orderBaseServiceFake = orderBaseService ?? Substitute.For<IActiveOrderService>();
            var doneOrderRepositoryFake = Substitute.For<IDoneOrdersRepository>();
            var doneOrderServiceFake = Substitute.For<IDoneOrderService>();
            var tradeExecutionServiceFake = Substitute.For<ITradeExecutionService>();

            Mapper.Initialize(cfg => cfg.AddProfile(new AutoMapperConfig()));

            accountInfoRepositoryFake.GetByIdAsync(Arg.Any<string>()).ReturnsForAnyArgs(AccountInfo.CreateDefault("1"));
            activeLimitOrdersRepositoryFake.AddAsync(Arg.Any<IOrderBase>()).ReturnsForAnyArgs(Task.FromResult);
            marketProfileRepositoryFake.GetByIdAsync(Arg.Any<string>()).ReturnsForAnyArgs(new AssetPairQuote());
            marketOrdersRepositoryFake.AddAsync(Arg.Any<IOrderBase>()).ReturnsForAnyArgs(Task.FromResult);
            marketOrdersRepositoryFake.GetByIdAsync(Arg.Any<string>()).ReturnsForAnyArgs(new OrderBase());
            marketOrdersRepositoryFake.DeteleAsync(Arg.Any<string>()).ReturnsForAnyArgs(Task.FromResult);
            assetPairDictionaryServiceFake.GetByIdAsync(Arg.Any<string>()).ReturnsForAnyArgs(new AssetPair());
            tradingAccountRepositoryFake.GetByClientId(Arg.Any<string>()).ReturnsForAnyArgs(new List<ITradingAccount>());
            tradingAccountRepositoryFake.UpdateAsync(Arg.Any<ITradingAccount>()).ReturnsForAnyArgs(Task.FromResult);
            orderCalculatorFake.CalculateProfitLossAsync(Arg.Any<double>(), Arg.Any<double>(), Arg.Any<double>(),
                Arg.Any<IAssetPair>(), Arg.Any<string>());
            accountServiceFake.GetAccountInfoAsync(Arg.Any<string>()).ReturnsForAnyArgs(new AccountInfoDto());
            matchingEngineEventsFake.AccountUpdatedAsync(Arg.Any<AccountInfoDto>()).ReturnsForAnyArgs(Task.FromResult);
            tradingAccountService.IsStopOutReachedAsync(Arg.Any<string>()).ReturnsForAnyArgs(false);
            orderBaseServiceFake.MoveOrderToDoneAsync(Arg.Any<IOrderBase>(), Arg.Any<OrderStatus>(),
                Arg.Any<OrderComment>(), Arg.Any<double>()).ReturnsForAnyArgs(Task.FromResult);
            doneOrderServiceFake.CreateDoneOrderAsync(Arg.Any<IOrderBase>(), Arg.Any<OrderStatus>(),
                Arg.Any<OrderComment>()).ReturnsForAnyArgs(new DoneOrder());
            tradeExecutionServiceFake.CheckTradeRequestAsync(Arg.Any<string>(), Arg.Any<string>(), Arg.Any<double>(),
                Arg.Any<double>()).ReturnsForAnyArgs(true);

            return new TradingAppService(accountInfoRepositoryFake, marketOrdersRepositoryFake,
                marketProfileRepositoryFake,
                orderCalculatorFake, matchingEngineEventsFake,
                activeLimitOrdersRepositoryFake, assetPairDictionaryServiceFake, tradingAccountRepositoryFake,
                accountServiceFake, tradingAccountService, orderBaseServiceFake, doneOrderRepositoryFake,
                tradeExecutionServiceFake, doneOrderServiceFake);
        }

        [Fact]
        public async Task ActiveOrder_IsCreated_IfNoDefinedPriceSpecified()
        {
            var openOrderContext = new OpenOrderContext
            {
                DefinedPrice = double.NaN
            };

            var activeLimitOrdersRepositoryFake = Substitute.For<IPendingOrdersRepository>();
            var marketOrdersRepositoryFake = Substitute.For<IActiveOrdersRepository>();

            //act
            var tradingService = CreateTradingService(marketOrdersRepositoryFake, activeLimitOrdersRepositoryFake, null,
                null, null);
            await tradingService.OpenOrderAsync(openOrderContext);


            //assert
            await marketOrdersRepositoryFake.Received().AddAsync(Arg.Any<IOrderBase>());
            await activeLimitOrdersRepositoryFake.DidNotReceive().AddAsync(Arg.Any<IOrderBase>());
        }

        [Fact]
        public async Task CloseOrder_UpdatesUserAccount()
        {
            var activeLimitOrdersRepositoryFake = Substitute.For<IPendingOrdersRepository>();
            var marketOrdersRepositoryFake = Substitute.For<IActiveOrdersRepository>();
            var tradingAccountRepositoryFake = Substitute.For<ITradingAccountRepository>();
            var matchingEngineEventsFake = Substitute.For<IMatchingEngineEvents>();
            var orderBaseServiceFake = Substitute.For<IActiveOrderService>();

            //act
            var tradingService = CreateTradingService(marketOrdersRepositoryFake, activeLimitOrdersRepositoryFake,
                tradingAccountRepositoryFake, orderBaseServiceFake, matchingEngineEventsFake);
            await tradingService.CloseOrderAsync(string.Empty, string.Empty);

            //assert
            await tradingAccountRepositoryFake.Received().UpdateAsync(Arg.Any<ITradingAccount>());
            await
                orderBaseServiceFake.Received()
                    .MoveOrderToDoneAsync(Arg.Any<IOrderBase>(), Arg.Any<OrderStatus>(), Arg.Any<OrderComment>(),
                        Arg.Any<double>());
            await matchingEngineEventsFake.Received().AccountUpdatedAsync(Arg.Any<AccountInfoDto>());
        }

        [Fact(Skip = "not currenlty implemented")]
        public async Task NewOrderIsNotOpened_IfStopOut_IsReached()
        {
            //arrange
            var accountInfoRepositoryFake = Substitute.For<IAccountInfoRepository>();
            var accountServiceFake = Substitute.For<IAccountAppService>();
            var activeLimitOrdersRepositoryFake = Substitute.For<IPendingOrdersRepository>();
            var assetPairDictionaryServiceFake = Substitute.For<IAssetPairDictionaryService>();
            var marketOrdersRepositoryFake = Substitute.For<IActiveOrdersRepository>();
            var marketProfileRepositoryFake = Substitute.For<IMarketProfileRepository>();
            var matchingEngineEventsFake = Substitute.For<IMatchingEngineEvents>();
            var orderCalculatorFake = Substitute.For<IOrderCalculator>();
            var tradingAccountRepositoryFake = Substitute.For<ITradingAccountRepository>();
            var tradingAccountService = Substitute.For<ITradingAccountService>();
            var activeOrderServiceFake = Substitute.For<IActiveOrderService>();
            var doneOrderRepositoryFake = Substitute.For<IDoneOrdersRepository>();
            var doneOrderServiceFake = Substitute.For<IDoneOrderService>();
            var tradeExecutionServiceFake = Substitute.For<ITradeExecutionService>();

            Mapper.Initialize(cfg => cfg.AddProfile(new AutoMapperConfig()));

            var newOrderContext = new OpenOrderContext
            {
                DefinedPrice = double.NaN
            };
            marketProfileRepositoryFake.GetByIdAsync(Arg.Any<string>()).ReturnsForAnyArgs(new AssetPairQuote());
            tradingAccountService.IsStopOutReachedAsync(Arg.Any<string>()).ReturnsForAnyArgs(true);
            activeOrderServiceFake.MoveOrderToDoneAsync(Arg.Any<IOrderBase>(), Arg.Any<OrderStatus>(),
                Arg.Any<OrderComment>(), Arg.Any<double>()).ReturnsForAnyArgs(Task.FromResult);
            marketOrdersRepositoryFake.AddAsync(Arg.Any<IOrderBase>()).ReturnsForAnyArgs(Task.FromResult);

            //act
            var tradingService = new TradingAppService(accountInfoRepositoryFake, marketOrdersRepositoryFake,
                marketProfileRepositoryFake, orderCalculatorFake, matchingEngineEventsFake,
                activeLimitOrdersRepositoryFake, assetPairDictionaryServiceFake, tradingAccountRepositoryFake,
                accountServiceFake, tradingAccountService, activeOrderServiceFake, doneOrderRepositoryFake,
                tradeExecutionServiceFake, doneOrderServiceFake);
            await tradingService.OpenOrderAsync(newOrderContext);

            //assert
            await activeOrderServiceFake.Received().MoveOrderToDoneAsync(Arg.Any<IOrderBase>(), Arg.Any<OrderStatus>(),
                Arg.Any<OrderComment>(), Arg.Any<double>());
        }

        [Fact]
        public async Task PendingOrder_IsCreated_IfNoDefinedPriceSpecified()
        {
            var openOrderContext = new OpenOrderContext
            {
                DefinedPrice = 5.0
            };

            var activeLimitOrdersRepositoryFake = Substitute.For<IPendingOrdersRepository>();
            var marketOrdersRepositoryFake = Substitute.For<IActiveOrdersRepository>();

            //act
            var tradingService = CreateTradingService(marketOrdersRepositoryFake, activeLimitOrdersRepositoryFake, null,
                null, null);
            await tradingService.OpenOrderAsync(openOrderContext);

            //assert
            await activeLimitOrdersRepositoryFake.Received().AddAsync(Arg.Any<IOrderBase>());
            await marketOrdersRepositoryFake.DidNotReceive().AddAsync(Arg.Any<IOrderBase>());
        }
    }
}