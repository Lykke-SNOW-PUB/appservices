﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Lykke.Terminal.BusinessService.Messaging.MessageHandlers;
using Lykke.Terminal.Domain.ApplicationServices.Exchange;
using Lykke.Terminal.Domain.Assets;
using Lykke.Terminal.Domain.Dictionaries;
using Lykke.Terminal.Domain.Exchange;
using Lykke.Terminal.Domain.Messaging.Dtos;
using Lykke.Terminal.Domain.Notifications;
using Lykke.Terminal.Messaging;
using NSubstitute;
using Xunit;

namespace Lykke.Terminal.BusinessService.Tests.Messaging
{
    public class OrderBookMessageHandlerTests
    {
        [Fact]
        public async Task OrderBook_IsUpdated_ForExistingAssetPairs_Only()
        {
            var orderBookRepositoryFake = Substitute.For<IOrderBookRepository>();
            orderBookRepositoryFake.GetByAssetPairIdAndBuy(Arg.Any<IOrderBook>())
                .ReturnsForAnyArgs(new List<IOrderBook>());
            var matchingEngineEventsFake = Substitute.For<IMatchingEngineEvents>();
            var marketProfileServiceFake = Substitute.For<IMarketProfileService>();
            var assetPairDictionaryServiceFake = Substitute.For<IAssetPairDictionaryService>();

            //act
            assetPairDictionaryServiceFake.GetByIdAsync(Arg.Any<string>()).ReturnsForAnyArgs((IAssetPair) null);
            var orderBookMessageHandler = new OrderBookMessageHandler(orderBookRepositoryFake, matchingEngineEventsFake,
                marketProfileServiceFake, assetPairDictionaryServiceFake);
            await orderBookMessageHandler.HandleAsync(new OrderBookMessageDto(), new BrokerMessageContext());

            //assert
            await orderBookRepositoryFake.DidNotReceiveWithAnyArgs().GetByAssetPairIdAndBuy(Arg.Any<IOrderBook>());
        }

        [Fact]
        public async Task OrderBook_SendsEvents()
        {
            var orderBookRepositoryFake = Substitute.For<IOrderBookRepository>();
            var matchingEngineEventsFake = Substitute.For<IMatchingEngineEvents>();
            var marketProfileServiceFake = Substitute.For<IMarketProfileService>();
            var assetPairDictionaryServiceFake = Substitute.For<IAssetPairDictionaryService>();

            assetPairDictionaryServiceFake.GetByIdAsync(Arg.Any<string>()).ReturnsForAnyArgs(new AssetPair());
            orderBookRepositoryFake.GetByAssetPairIdAndBuy(Arg.Any<IOrderBook>())
                .ReturnsForAnyArgs(new List<IOrderBook>());
            orderBookRepositoryFake.AddAsync(Arg.Any<IOrderBook>()).ReturnsForAnyArgs(Task.FromResult);
            matchingEngineEventsFake.OrderBookUpdatedAsync(Arg.Any<OrderBookMessageDto>())
                .ReturnsForAnyArgs(Task.FromResult);
            marketProfileServiceFake.UpdateMarketProfileAsync(Arg.Any<IOrderBook>()).ReturnsForAnyArgs(Task.FromResult);
            Mapper.Initialize(cfg => cfg.AddProfile(new AutoMapperConfig()));

            //act
            var orderBookMessageHandler = new OrderBookMessageHandler(orderBookRepositoryFake, matchingEngineEventsFake,
                marketProfileServiceFake, assetPairDictionaryServiceFake);
            await orderBookMessageHandler.HandleAsync(new OrderBookMessageDto {Prices = new List<OrderBookDetailsDto>()}, new BrokerMessageContext());

            //assert
            await matchingEngineEventsFake.Received().OrderBookUpdatedAsync(Arg.Any<OrderBookMessageDto>());
            await marketProfileServiceFake.Received().UpdateMarketProfileAsync(Arg.Any<IOrderBook>());
        }

        [Fact]
        public async Task OrderBook_UpdatesExistingData_IfExists()
        {
            var orderBookRepositoryFake = Substitute.For<IOrderBookRepository>();
            var matchingEngineEventsFake = Substitute.For<IMatchingEngineEvents>();
            var marketProfileServiceFake = Substitute.For<IMarketProfileService>();
            var assetPairDictionaryServiceFake = Substitute.For<IAssetPairDictionaryService>();

            assetPairDictionaryServiceFake.GetByIdAsync(Arg.Any<string>()).ReturnsForAnyArgs(new AssetPair());
            orderBookRepositoryFake.GetByAssetPairIdAndBuy(Arg.Any<IOrderBook>())
                .ReturnsForAnyArgs(new List<IOrderBook> {new OrderBook()});
            orderBookRepositoryFake.UpdateAsync(Arg.Any<IOrderBook>()).ReturnsForAnyArgs(Task.FromResult);
            matchingEngineEventsFake.OrderBookUpdatedAsync(Arg.Any<OrderBookMessageDto>())
                .ReturnsForAnyArgs(Task.FromResult);
            marketProfileServiceFake.UpdateMarketProfileAsync(Arg.Any<IOrderBook>()).ReturnsForAnyArgs(Task.FromResult);
            Mapper.Initialize(cfg => cfg.AddProfile(new AutoMapperConfig()));

            //act
            var orderBookMessageHandler = new OrderBookMessageHandler(orderBookRepositoryFake, matchingEngineEventsFake,
                marketProfileServiceFake, assetPairDictionaryServiceFake);
            await orderBookMessageHandler.HandleAsync(new OrderBookMessageDto { Prices = new List<OrderBookDetailsDto>() }, new BrokerMessageContext());

            //assert
            await orderBookRepositoryFake.Received().UpdateAsync(Arg.Any<IOrderBook>());
        }
    }
}